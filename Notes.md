# Concernant le terrain :

Terrain composé de blocs, comme dans Terraria ou Starbound,
Une même texture utilisée pour le fond d'un type de terrain, sur toute sa surface,
La forme d'un bloc dépend de ses voisins, de façon à ce que ça paraisse lissé.

 -   -   -   -  
+•+ -•+ +•+ -•-  (...)
 +   +   -   +  

# La physique ?

La comme ça, je vois la boucle suivante :
- Application d'effets, mise à jour des mouvements
- Calcul des mouvements,
- Gestion des collisions, correction des positions, ajout d'effets (comme des rebonds)

Mais je vois déjà un problème, à savoir qu'un rebond "interromprait" un mouvement le temps d'une frame.

# Physique des ombres

Chaque entité aurait une (normal map ?) indiquant pour chaque pixel son angle (un vecteur x,y).
Chaque entité aurait donc aussi un filtre de couleurs, saturation et luminosité en fonction des sources de lumière et de leurs origines.

# Réflexion sur la physique du gameplay

Là, comme ça, je verrais le gameplay comme :
- Permissif, fluide pour le parkour,
- Exigent pour les combats

Je repense à une vidéo où l'un des créateurs de Dead Cells disait faire tricher le joueur sur certains points, notamment quand on saute d'une plateforme. Si le joueur saute un chouilla trop tard, c'est à dire alors qu'il est déjà dans le vide, eh bien ça passe quand même.
En fait, je m'étais déjà fait une réflexion sur les considérations du joueur quant aux "imprécisions" du jeu. Un jeu peut manquer de réalisme sur plusieurs points. Si ce manque facilite la vie du joueur, ou n'y changera rien, ça passera plus ou moins inaperçu. Au mieux, une remarque humoristique. Dans le cas contraire, cela provoquera une frustration que l'on souhaite éviter.
Le manque de réalisme pour le cas de sauter d'une plateforme mais en fait non car on saute trop tard, c'est que dans la vraie vie, une telle chose n'arriverait pas. Mécaniquement, notre pied trouverait forcément le coin de la fin de la plateforme.
En revanche, pouvoir sauter trop tard ne doit pas permettre de sauter plus loin. Cela doit fonctionner comme une sorte de correction rétro-active.

