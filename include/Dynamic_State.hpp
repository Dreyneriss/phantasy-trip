
//include guard
#ifndef DYNAMIC_STATE_HPP
#define DYNAMIC_STATE_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>

#include "Playable.hpp"
#include "Entity_Char.hpp"

class Entity_Char;

class Dynamic_State : public Playable {
	protected:
		Entity_Char * _char;

		std::vector<std::vector<float>> _spriteCycle;
		std::vector<std::vector<std::vector<float>>> _texCoords;

		bool _available;

		sf::Time _time;
		sf::Clock _clock;

	public:

		Dynamic_State();
		//~Dynamic_State(); // TO DO

		void setChar(Entity_Char * c);

		virtual sf::Time getTime();
		virtual bool isAvailable();
		virtual void setAvailable(bool available);
		//virtual void lock(); // = setAvailable(false)
		//virtual void free/open(); // = setAvailable(true)
		// virtual sf::Time readyIn(); // every state could have reason for not be available
		// a Time value at Zero or below would mean it's ready
		// an always ready State would return sf::Time::Zero

		virtual void setSprite(int i, int j);
		virtual void init(){}
		virtual void update(){}
};

class Standing_State : public Dynamic_State{
  protected:
    float _baseSpeed;
    float _jumpSpeed;

		bool _walking;

  public:
    Standing_State();

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();

		void init();
		void update();
};

class Jumping_State : public Dynamic_State{
  protected:
    float _baseSpeed;
    float _jumpSpeed;

		sf::Time _limit;
		sf::Time _cooldown;

		bool _jumping;

  public:
    Jumping_State();

		sf::Time getTime();

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();

		void init();
		void update();
};

class AirJumping_State : public Jumping_State{
  protected:
    float _baseSpeed;
    float _jumpSpeed;

		sf::Time _limit;
		sf::Time _cooldown;

		bool _jumping;

  public:
    AirJumping_State();

		sf::Time getTime();

		void c_jump();

		void init();
		void update();
};

class Falling_State : public Dynamic_State{
  protected:
    float _baseSpeed;
    float _jumpSpeed;

  public:
    Falling_State();

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();

		void init();
		void update();
};

class WallJumping_State : public Dynamic_State{
	protected:
		float _baseSpeed;
		float _jumpSpeed;

		sf::Time _duration;
		sf::Time _cooldown;

		bool _first;
		bool _second;

	public:
		WallJumping_State();

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();

		bool isAvailable();
		void setAvailable(bool available);

		void init();
		void update();
};

class WallGrab_State : public Dynamic_State{
	protected:
		float _baseSpeed;
		float _jumpSpeed;

		int _i;
		int _j;

		bool _grab;
		bool _lift;

	public:
		WallGrab_State();

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();

		void init();
		void update();
};

#endif
