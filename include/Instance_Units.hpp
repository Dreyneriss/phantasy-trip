
//include guard
#ifndef INSTANCE_UNITS_HPP
#define INSTANCE_UNITS_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>

#include "Instance.hpp"
#include "World.hpp"
#include "Entity_Char.hpp"
#include "InputHandler.hpp"

class Entity_Char;
class World;

class Instance_Units : public Instance {
	protected:
		std::vector<Entity_Char *> _char;
		World * _world;

		// Cells management
		std::vector<std::vector<int>> _map_id;
		std::vector<std::vector<int>> _map_cond;
		int _map_width;
		int _map_heigh;

		int _unitPerTile;
	  int _pixelPerUnit;
	  int _pixelPerTile; // = _unitPerTile * _pixelPerUnit
		int _tileStandard;
		int _meterStandard;
		float _meterPerTile;
		float _tilePerMeter;

		float _gravAcc; // dist/sec
		float _gravMax; // for realism purpose, should depend of one's weight and aerodynamism

		//Entity_Menu _menu; //inherit Entity

		Entity_Char * _focus; //for the inputs handling ; may also get a significant NULL value
													// Entity_Test because it inherits Playable
		float _zoom;
		sf::Vector2f _screenCenter;
		sf::Vector2i _mouse;

		InputHandler _inputHandler;

	public:

		Instance_Units(sf::RenderWindow * window, sf::Vector2f _screenSize);
		//~Instance_Units(); // TO DO

		void addWorldUnit(int width, int heigh);
		void addCharacter(float x, float y);

		World * getWorld();
		Entity_Char * getFocused(){return _focus;}
		sf::Vector2i getMousePos(){return _mouse;}

		void switchFocused();

		// Physics

		void applyGravity(Entity_Char* ent, sf::Time _turnTime);
		int getUnitPerTile(){return _unitPerTile;}
		int getPixelPerTile(){return _pixelPerTile;}
		int getPixelPerUnit(){return _pixelPerUnit;}

		bool isSubTileFree(int i, int j);

		// INSTANCE METHODS

		float getZoom(){return _zoom;}
		void zoom(float z){_zoom *= z;}

		void setScreenCenter(float ratioX, float ratioY); //x and y being between 0 and 1
		sf::Vector2f getScreenCenter(){return _screenCenter;}

		void update(sf::RenderWindow & window, sf::Event & event, sf::Time _turnTime);

		void draw(sf::RenderWindow & window);
};

#endif
