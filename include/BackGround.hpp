

//include guard
#ifndef BACKGROUND_HPP
#define BACKGROUND_HPP


//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>
#include <sstream> // https://stackoverflow.com/questions/7868936/read-file-line-by-line-using-ifstream-in-c


class BackGround : public sf::Drawable, public sf::Transformable
{
	private:
		sf::VertexArray _vertices;//(sf::Quads, 4);
		sf::Texture _tex;
		std::string _name;
		int _id;
		sf::RenderStates _states;
		
		sf::Vector2f & _position;
		
		std::vector<std::vector<int>> & _map_id;
		std::vector<std::vector<int>> & _map_cond;
		
		int & _width;
		int & _heigh;
		int & _nbCells;
		
		
	public:
		
  BackGround(std::vector<std::vector<int>> &, std::vector<std::vector<int>> &, int &, int &, int &, sf::Vector2f &);
		//~BackGround(/**/); //TODO
		
		void SetTexture(std::string adress);
		void SetName(std::string name);
		void SetId(int id);
		
		void SetVertices();
		
		void move(int, int);
		
		void draw(sf::RenderTarget & target, sf::RenderStates states) const;
};

#endif
