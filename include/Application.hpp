/**
DESCRIPTION
This class mainly contains the Window application loop.
It also contains an Instance type object.
**/

//include guard
#ifndef APPLICATION_HPP
#define APPLICATION_HPP

//included dependencies
#include "SFML/Graphics.hpp"	//RenderWindow
#include "SFML/Window.hpp"		//Window - VideoMode - Style
#include "SFML/System.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <random>

#include "Instance.hpp"
#include "Instance_Units.hpp"

class Application{
	private:
    // Window attributes
    int _width;
		int _heigh;
    std::string _name;

    int _frameRate;

    sf::Clock _globalTime;
  	sf::Time _elapsedTime;

    sf::RenderWindow _window;

    //
    Instance * _inst;

	public:

		Application(int x, int y, std::string name);
		//~Application(/**/); //TODO?

    void setSize(int x, int y);
    void setName(std::string name);
    void setFrameRate(int);

    void init();
    void run();
    void close();
};

#endif
