

//include guard
#ifndef TERRAIN_HPP
#define TERRAIN_HPP


//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>
#include <sstream> // https://stackoverflow.com/questions/7868936/read-file-line-by-line-using-ifstream-in-c

#include "World.hpp"

class World;

class Terrain : public sf::Drawable, public sf::Transformable
{
	private:
		//sf::VertexArray _vertices;//(sf::Quads, 4);
		std::vector<std::vector<sf::VertexArray>> _vertices;//(sf::Quads, 4);
		sf::Texture _tex;
		std::string _name;
		int _id;
		sf::RenderStates _states;

		sf::Vector2i _tilesPerPannel;
		int _pannelWidth;
		int _pannelHeigh;

		std::vector<std::vector<int>> * _map_ids;
		std::vector<std::vector<int>> * _map_tiles;

		int _width;
		int _heigh;
		int _nbCells;

		World * _world;


	public:

		Terrain(World*);
		//~Terrain(/**/); //TODO

		void SetTexture(std::string adress);
		void SetName(std::string name);
		void SetId(int id);

		void SetVertices(int nbX, int nbY);
		void updateVerticesAt(int i, int j);

		void draw(sf::RenderTarget & target, sf::RenderStates states) const;
};

#endif
