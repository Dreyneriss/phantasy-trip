
//include guard
#ifndef INSTANCE_HPP
#define INSTANCE_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>


class Instance{
	protected:
		sf::Time _turnTime;

		sf::RenderWindow * _window;

		sf::Vector2f _screenSize;

	public:

		Instance(sf::RenderWindow * window, sf::Vector2f screenSize);
		//~Instance(/**/); //TODO

		sf::Vector2f getScreenSize(){return _screenSize;}
		void setScreenSize(sf::Vector2f size){_screenSize = size;}

		virtual void update(sf::RenderWindow & window, sf::Event & event, sf::Time _turnTime);

    virtual void draw(sf::RenderWindow & window);
};

#endif
