
//include guard
#ifndef ENTITY_CHAR_HPP
#define ENTITY_CHAR_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow

enum State_Type{
	STANDING_STATE,
	RUNNING_STATE,
	JUMPING_STATE,
	AIRJUMPING_STATE,
	WALLJUMPING_STATE,
	WALLGRAB_STATE,
	FALLING_STATE
};

#include "Entity.hpp"
#include "Playable.hpp"
#include "Instance_Units.hpp"
#include "Dynamic_State.hpp"

class Instance_Units;
class Dynamic_State;
class Standing_State;
class Jumping_State;
class AirJumping_State;
class WallJumping_State;
class WallGrab_State;
class Falling_State;
class Running_State;

class Entity_Char : public Entity, public Playable, public sf::Drawable, public sf::Transformable
{
	protected:

		Dynamic_State * _state;
		Standing_State * _standing_state;
		Jumping_State * _jumping_state;
		AirJumping_State * _air_jumping_state;
		WallJumping_State * _wall_jumping_state;
		WallGrab_State * _wall_grab_state;
		Falling_State * _falling_state;
		/*Running_State _running_state;*/
		void setStates();

		// Terraforming_State * _terra_state
		// Fighting_State * _fight_state
		// Social_State * _social_state
		// Dynamic_State * _secondary_state
		//  OR
		// _state->takeState(_secondary_state);
		// Dynamic_State::update(){
		//	if(_state != NULL)
		//		_state->update();
		// }

		sf::VertexArray _shape;
		sf::Texture _tex;
		bool _texInvert;

    sf::Vector2f _pos0; //_pos already exists
		float _feetExtraSize;

		float _baseSpeed;
		float _jumpSpeed;
		sf::Vector2f _speed; //distance per second
		sf::Vector2f _vect1; //computed from _speed and the delta time
		sf::Vector2f _vect2; //computed if the move is non-straight (mid-move collision that doesn't completely stop the entity)

		Instance_Units * _inst;

		// TERRAFORMING
		bool _leftClic0;
		bool _leftClic;
		bool _addingTiles;
		int _selectId;

	public:

		Entity_Char();
		Entity_Char(float x, float y, Instance_Units * inst);
		//~Entity_Test(/**/); //TODO

		void setInstance(Instance_Units* inst);
		Instance_Units * getInst(){return _inst;}

		//##########################
		// COMMANDS

		void c_left();
		void c_right();
		void c_up();
		void c_down();
		void c_jump();
		void c_plus();
		void c_minus();
		void c_Lclic();
		void c_Rclic();
		void c_Mclic();
		void c_T1clic();
		void c_T2clic();

		//##########################
		// STATES MANAGMENT

		void setState(State_Type state);
		Dynamic_State * getCurrentState();
		Dynamic_State * getState(State_Type state);

		//##########################
		// BODY MANAGMENT

		void setPos(sf::Vector2f pos);
    void move();
		void moveOnFloor();
    void moveUp(sf::Vector2f vect);
    void moveDown(sf::Vector2f vect);
    void moveRight(sf::Vector2f vect);
		void moveRightOnFloor(sf::Vector2f vect);
    void moveLeft(sf::Vector2f vect);
		void moveLeftOnFloor(sf::Vector2f vect);

    void setShape(float x0, float x1, float y0, float y1);
		void setTexZone(float x0, float x1, float y0, float y1);
		bool isTexInverted();
		void setTexInvertion(bool invert);

		bool canMoveLeft();
		bool canStepUpLeft();
		bool canStepDownLeft();
		void checkLeftPossibilities(bool & left, bool & upLeft, bool & downLeft);
		bool canMoveRight();
		bool canStepUpRight();
		bool canStepDownRight();
		void checkRightPossibilities(bool & right, bool & upRight, bool & downRight);
		bool canMoveUp();
		bool canMoveDown(bool extraFeet = false);

		bool canWallRunLow();
		bool canWallRunHigh();
		bool canWallGrab();
		bool canWallGrab(int & i, int & j);

		void setBaseSpeed(float speed);
		void setJumpSpeed(float speed);

		float getBaseSpeed();
		sf::Vector2f getSpeed();

		void setHoriSpeed(float speed);
		void setVertSpeed(float speed);

		void moveHoriSpeed(float speed);
		void moveVertSpeed(float speed);

		//can be put in the base Entity class
		bool AABBCollision(sf::Vector2f bPos, sf::Vector2f bSize, sf::Vector2f & shortVect, sf::Vector2f & normale);

		//#########################
		// GAME STEPS

		void play(sf::Event & event, sf::Time _turnTime);

		void update(sf::Time _turnTime);

    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
};

#endif
