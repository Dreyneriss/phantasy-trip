
//include guard
#ifndef ENTITY_HPP
#define ENTITY_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>

enum Direction{North, South, East, West, NEast, SEast, NWest, SWest};

class Entity{
	public:

		enum ShapeType{Circle, Rectangle, Square, Triangle, Point};

		Direction condToDir(int cond){
			switch(cond){
				case 5 : { // above & left : NWest block
					return NWest;
					break;
				}case 6 : { // above & right : NEast block
					return NEast;
					break;
				}case 9 : { // bellow & left : SWest block
					return SWest;
					break;
				}case 10 : { // bellow & right : SEast block
					return SEast;
					break;
				}
			}
		}

		ShapeType _type;

	protected:
		sf::Time _turnTime;

		sf::Vector2f _pos;
		sf::Vector2f _size;

	public:

		Entity();
		//~Entity(); // TO DO

		//########################
		// BODY MANAGMENT

		virtual void setPos(sf::Vector2f pos);
		virtual sf::Vector2f getPos();

		virtual void setSize(sf::Vector2f size);
		virtual sf::Vector2f getSize();

    virtual void move(sf::Vector2f pos);

		//virtual ShapeType getShapeType();

		//#########################
		// GAME STEPS

    virtual void play(sf::Event & event, sf::Time _turnTime);

		virtual void update(sf::Time _turnTime);

    virtual void draw(sf::RenderWindow & window);
};

#endif
