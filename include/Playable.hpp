
//include guard
#ifndef PLAYABLE_HPP
#define PLAYABLE_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

class Playable
{
	protected:


	public:

		//Playable();
		//~Entity_Test(/**/); //TODO

		//##########################
		// COMMANDS

		virtual void c_jump(){}
    virtual void c_up(){}
    virtual void c_down(){}
    virtual void c_left(){}
    virtual void c_right(){}
    virtual void c_plus(){}
    virtual void c_minus(){}
    virtual void c_Lclic(){}
    virtual void c_Rclic(){}
    virtual void c_Mclic(){}
    virtual void c_T1clic(){}
    virtual void c_T2clic(){}


};

#endif
