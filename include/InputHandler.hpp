
//include guard
#ifndef INPUTHANDLER_HPP
#define INPUTHANDLER_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow

#include "Command.hpp"
#include "Playable.hpp"

//############################
// INPUT HANDLER

class InputHandler
{
  public:
    InputHandler();
    ~InputHandler();

    void handleInput(Playable* actor);

    // Methods to bind commands...

    void initDefault();

  private: //gonna do this for every keyboard keys ? Seems stupid
    Command* _keyZ;
    Command* _keyQ;
    Command* _keyS;
    Command* _keyD;
    Command* _keyUp;
    Command* _keyDown;
    Command* _keyRight;
    Command* _keyLeft;
    Command* _keySpace;
    Command* _keyShift;
    Command* _keyCtrl;
    Command* _keyEntr;
    Command* _keyPlus;
    Command* _keyMinus;
    Command* _mouseRight;
    Command* _mouseLeft;
    Command* _mouseMiddle;
    Command* _mouseThumb1;
    Command* _mouseThumb2;

    Command* _cJump;
    Command* _cUp;
    Command* _cDown;
    Command* _cLeft;
    Command* _cRight;
    Command* _cPlus;
    Command* _cMinus;
    Command* _cRClic;
    Command* _cLClic;
    Command* _cMClic;
    Command* _cT1Clic;
    Command* _cT2Clic;

};

#endif
