
//include guard
#ifndef WORLD_HPP
#define WORLD_HPP


//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>
#include <sstream> // https://stackoverflow.com/questions/7868936/read-file-line-by-line-using-ifstream-in-c

#include "Terrain.hpp"
#include "Instance_Units.hpp"

class Terrain;
class Instance_Units;

class World{
	private:
		// Cells management
		std::vector<std::vector<int>> _map_ids;
		std::vector<std::vector<int>> _map_tiles;

		int _width;
		int _heigh;
		int _nbCells;

		// Graphics management
		//std::vector<BackGround> _backs;
		std::vector<Terrain> _terrains;
		//std::vector<Details> _details; //do not have to be a vector... doing severals Details objects seems pointless

		sf::RenderWindow * _window;
		Instance_Units * _inst;

	public:

		World();
		World(sf::RenderWindow * window);
		//~World(/**/); //TODO

		void setInst(Instance_Units * inst);

		void addTerrain(std::string name, int id, std::string tex);
		void LoadMapFromPPM(std::string adress);
		void computeTilesShapes();
		void SetVertices();

		void setIdAt(int id, int i, int j);
		void updateTypesAt(int i, int j);
		void updateVerticesAt(int i, int j);

		int getIdAt(int i, int j);
		int getTypeAt(int i, int j);
		int getPixelPerTile();
		int getUnitPerTile();

		bool isSubTileFree(int i, int j);

		sf::Vector2f getScreenCenter();
		float getZoom();
		sf::Vector2f getFocusedPos();

		//void setTerrainTex(int id, std::string tex);
		//void setTerrainTex(std::string name, std::string tex);

		void draw(sf::RenderWindow & window);
};

#endif
