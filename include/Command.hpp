
//include guard
#ifndef COMMAND_HPP
#define COMMAND_HPP

//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

#include "Playable.hpp"

// platformer moves
/*
Reflexions

Every commands methods in Entity, like Entity::jump, etc, but initialized empty
Or only the most basics like Entity::move, and other more specifics in Entity_subclasses
Or a special class, Playable, getting every methods, and Entity_subclass may inherit from Playable

*/

class Command
{
public:
  virtual ~Command() {}
  virtual void execute(){};// = 0;
  virtual void execute(Playable* actor){};// = 0;
};

class JumpCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_jump();
    }
};

// Directional commands

class UpCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_up();
    }
};

class DownCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_down();
    }
};

class LeftCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_left();
    }
};

class RightCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_right();
    }
};

class PlusCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_plus();
    }
};

class MinusCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_minus();
    }
};

class LeftClicCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_Lclic();
    }
};

class RightClicCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_Rclic();
    }
};

class MiddleClicCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_Mclic();
    }
};

class Thumb1ClicCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_T1clic();
    }
};

class Thumb2ClicCommand : public Command
{
  public:
    void execute(Playable* actor)
    {
      actor->c_T2clic();
    }
};


#endif
