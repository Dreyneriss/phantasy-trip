# https://fr.wikipedia.org/wiki/Bruit_de_Perlin#Pseudo-code
# exec(open("./perlin.py").read())

import numpy as np
import math

# Precomputed (or otherwise) gradient vectors at each grid node
IYMAX = 100
IXMAX = 100
Gradient = np.random.rand(IYMAX,IXMAX,2)*100

# Function to linearly interpolate between a0 and a1
# Weight w should be in the range [0.0, 1.0]
def lerp(a0, a1, w):
	return (1.0 - w)*a0 + w*a1

# Computes the dot product of the distance and gradient vectors.
def dotGridGradient(ix, iy, x, y):
	#Compute the distance vector
	dx = x - ix
	dy = y - iy
	#Compute the dot-product
	return(dx*Gradient[iy][ix][0] + dy*Gradient[iy][ix][1])

# Compute Perlin noise at coordinates x, y
def perlin(x,y):
	#Determine grid cell coordinates
	x0 = math.floor(x)
	x1 = x0+1
	y0 = math.floor(y)
	y1 = y0+1
	#Determine interpolation weights
	#Could also use higher order polynomial/s-curve here
	sx = x - x0
	sy = y - y0
	#Interpolate between grip point gradients
	n0 = dotGridGradient(x0,y0,x,y)
	n1 = dotGridGradient(x1,y0,x,y)
	ix0 = lerp(n0,n1,sx)
	n0 = dotGridGradient(x0,y1,x,y)
	n1 = dotGridGradient(x1,y1,x,y)
	ix1 = lerp(n0,n1,sx)
	value = lerp(ix0,ix1,sy)
	return value

r = np.array(range(10))
r1 = r/10 + 7
r2 = r/10 + 3

m = np.array(range(50))
m1 = m/10 + 7
m2 = m/10 + 3

fullNum = 990
fullRange = np.array(range(fullNum))/10

r12 = np.zeros((10,)*2)
m12 = np.zeros((50,)*2)
fullMap = np.zeros((fullNum,)*2)
for i in range(10):
	for j in range(10):
		r12[i][j] = perlin(r1[i],r2[j])
for i in range(50):
	for j in range(50):
		m12[i][j] = perlin(m1[i],m2[j])
for i in range(fullNum):
	for j in range(fullNum):
		fullMap[i][j] = perlin(fullRange[i],fullRange[j])
avg = np.average(r12) #with big numbers, avg tends to zero
bot = np.min(fullMap)
top = np.max(fullMap)
decile = (np.array(range(1,11))*(top - bot)/10) + bot
centile = (np.array(range(1,101))*(top - bot)/100) + bot

print('P3')
print('# Created by a Python script')
print(fullNum, ' ', fullNum)
print('255')

for i in range(fullNum):
	for j in range(fullNum):
		if(fullMap[i][j] > centile[55]):
			print('0')
			print('0')
			print('0')
		else:
			print('255')
			print('255')
			print('255')
print('')
print('')

print('P3')
print('# Created by a Python script')
print(fullNum, ' ', fullNum)
print('255')

for i in range(fullNum):
	for j in range(fullNum):
		if(fullMap[i][j] > centile[50]):
			print('0')
			print('0')
			print('0')
		else:
			print('255')
			print('255')
			print('255')

#for i in range(fullNum):
#	for j in range(fullNum):
#		if(fullMap[i][j] > centile[55]):
#			print('x', end='')
#		else:
#			print(' ', end='')
#	print('')
#print('')
#print('')

#for i in range(fullNum):
#	for j in range(fullNum):
#		if(fullMap[i][j] > centile[50]):
#			print('x', end='')
#		else:
#			print(' ', end='')
#	print('')

#for i in range(50):
#	for j in range(50):
#		if(fullMap[i][j] > centile[55]):
#			print('x', end='')
#		else:
#			print(' ', end='')
#	print('')
#print('')

#for i in r1:
#	for j in r2:
#		print(perlin(i,j))

