#include "Dynamic_State.hpp"

Dynamic_State::Dynamic_State(){}

void Dynamic_State::setChar(Entity_Char * c){
  _char = c;
}

sf::Time Dynamic_State::getTime(){
  return _time;
}
bool Dynamic_State::isAvailable(){
  return _available;
}
void Dynamic_State::setAvailable(bool available){
  _available = available;
}

void Dynamic_State::setSprite(int i, int j){
  float t_x0,t_x1,t_y0,t_y1,offset_x,offset_y;
  t_x0 = _texCoords[i][j][0];
  t_x1 = _texCoords[i][j][1];
  t_y0 = _texCoords[i][j][2];
  t_y1 = _texCoords[i][j][3];
  offset_x = _texCoords[i][j][4];
  offset_y = _texCoords[i][j][5];

  float width = (t_x1 - t_x0) * 16 / 160;
  float heigh = (t_y1 - t_y0) * 16 / 160;

  if(_char->isTexInverted()){
    _char->setShape(width +offset_x, offset_x, offset_y, heigh -offset_y);
    _char->setTexZone(t_x0,t_x1,t_y0,t_y1);
  }else{
    _char->setShape(offset_x, width +offset_x, offset_y, heigh -offset_y);
    _char->setTexZone(t_x0,t_x1,t_y0,t_y1);
  }
}
