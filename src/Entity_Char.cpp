#include "Entity_Char.hpp"
#include <cmath>


Entity_Char::Entity_Char(){

  _shape = sf::VertexArray(sf::Quads, 4);

  _size.x = 1.2*16;
  _size.y = 4.9*16;

  //vertices and texCoords are set so that the shape's origin is to the bottom instead of the top
  // so that the origin correspond to the feet

  _tex.loadFromFile("./data/sprites/characters/characters.png");
  _tex.setSmooth(true);

  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  setStates();
  _state = _standing_state;
  _state->init();
}

Entity_Char::Entity_Char(float x, float y, Instance_Units * inst){
  _leftClic = false;
  _selectId = 0;

  _shape = sf::VertexArray(sf::Quads, 4);

  _inst = inst;

  _size.x = 1.2*16;
  _size.y = 4.9*16;
  _feetExtraSize = 4.5;

  //vertices and texCoords are set so that the shape's origin is to the bottom instead of the top
  // so that the origin correspond to the feet

  _tex.loadFromFile("./data/sprites/characters/characters.png");
  _tex.setSmooth(true);

  setPos(sf::Vector2f(x,y));

  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  setStates();
  _state = _standing_state;
  _state->init();
}

void Entity_Char::setStates(){
  _standing_state = new Standing_State();
  _jumping_state = new Jumping_State();
  _falling_state = new Falling_State();
  _wall_jumping_state = new WallJumping_State();
  _wall_grab_state = new WallGrab_State();
  _air_jumping_state = new AirJumping_State();

  _standing_state->setChar(this);
  _jumping_state->setChar(this);
  _air_jumping_state->setChar(this);
  _wall_jumping_state->setChar(this);
  _wall_grab_state->setChar(this);
  _falling_state->setChar(this);
  //_running_state.setChar(this);
}

void Entity_Char::setInstance(Instance_Units* inst){
  _inst = inst;
}

//##########################
// COMMANDS

void Entity_Char::c_left(){
  _state->c_left();
}
void Entity_Char::c_right(){
  _state->c_right();
}
void Entity_Char::c_up(){
  _state->c_up();
}
void Entity_Char::c_down(){
  _state->c_down();
}

void Entity_Char::c_jump(){
  _state->c_jump();
}

void Entity_Char::c_plus(){
  //_state->c_plus(); // useless
  _inst->zoom(1.01);
}
void Entity_Char::c_minus(){
  //_state->c_minus();
  _inst->zoom(0.99);
}

void Entity_Char::c_Lclic(){
  _leftClic = true;
}
void Entity_Char::c_Rclic(){
  //STUPID AS FUCK BUT RIGHT NOW I DON'T CARE
  _selectId = (_selectId + 1)%2;
}
void Entity_Char::c_Mclic(){
  _inst->switchFocused();
}
void Entity_Char::c_T1clic(){
  //doesn't work
}
void Entity_Char::c_T2clic(){
  //doesn't work
}

//##########################
// STATES MANAGMENT

void Entity_Char::setState(State_Type state){
  switch(state){
    case STANDING_STATE :{
      _state = _standing_state;
      _state->init();

      break;
    }
    case JUMPING_STATE :{
      _state = _jumping_state;
      _state->init();

      break;
    }
    case AIRJUMPING_STATE :{
      _state = _air_jumping_state;
      _state->init();

      break;
    }
    case WALLJUMPING_STATE :{
      _state = _wall_jumping_state;
      _state->init();

      break;
    }
    case WALLGRAB_STATE :{
      _state = _wall_grab_state;
      _state->init();

      break;
    }
    case FALLING_STATE :{
      _state = _falling_state;
      _state->init();

      break;
    }
  }
}
Dynamic_State * Entity_Char::getCurrentState(){
  return _state;
}
Dynamic_State * Entity_Char::getState(State_Type state){
  switch(state){
    case STANDING_STATE :{
      return _standing_state;
    }
    case JUMPING_STATE :{
      return _jumping_state;
    }
    case AIRJUMPING_STATE :{
      return _air_jumping_state;
    }
    case WALLJUMPING_STATE :{
      return _wall_jumping_state;
    }
    case WALLGRAB_STATE :{
      return _wall_grab_state;
    }
    case FALLING_STATE :{
      return _falling_state;
    }
  }
}

//##########################
// BODY MANAGMENT

void Entity_Char::setPos(sf::Vector2f pos){
  _pos0 = pos;
  _pos = pos;
}

/*
Right now, my character stay on a surface because his everfalling movement
 is converted in an horizontal movement.
I must get rid of that system because it fucks up with diagonal surfaces.
Adding flags for in-contact surfaces ?
Importing a real collision-management library ?
I don't know.
*/
// awesome tutorial :: https://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169
void Entity_Char::move(){
  sf::Vector2f vect;
  vect.x = _speed.x*_turnTime.asSeconds();
  vect.y = _speed.y*_turnTime.asSeconds();

  if(vect.x > 0)
    moveRight(sf::Vector2f(vect.x,0));
  else if(vect.x < 0)
    moveLeft(sf::Vector2f(vect.x,0));

  if(vect.y > 0)
    moveDown(sf::Vector2f(0,vect.y));
  else if(vect.y < 0)
    moveUp(sf::Vector2f(0,vect.y));
}
void Entity_Char::moveOnFloor(){
  sf::Vector2f vect;
  vect.x = _speed.x*_turnTime.asSeconds();
  vect.y = _speed.y*_turnTime.asSeconds();

  if(vect.x > 0)
    moveRightOnFloor(sf::Vector2f(vect.x,0));
  else if(vect.x < 0)
    moveLeftOnFloor(sf::Vector2f(vect.x,0));

  if(vect.y > 0)
    moveDown(sf::Vector2f(0,vect.y));
  else if(vect.y < 0)
    moveUp(sf::Vector2f(0,vect.y));
}
void Entity_Char::moveDown(sf::Vector2f vect){
  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if(_pos.y != float(int(_pos.y))){
    float gap = 1 - (_pos.y - float(int(_pos.y)));
    if(vect.y >= gap){
      vect.y -= gap;
      _pos.y = float(int(_pos.y)) +1;
    }else{
      _pos.y += gap;
      vect.y = 0;
    }
  }
  //MOVING SUBTILE PER SUBTILE
  float step;
  while(vect.y > 0){
    if(vect.y<1){
      step = vect.y;
      vect.y = 0;
    }else{
      step = 1;
      vect.y -= 1;
    }
    if(canMoveDown()){
      _pos.y += step;
    }
  }
}
void Entity_Char::moveUp(sf::Vector2f vect){
  vect.y *= -1; // to simplify

  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if((_pos.y-_size.y) != float(int(_pos.y-_size.y))){
    float gap = _pos.y - _size.y - float(int(_pos.y-_size.y));
    if(vect.y >= gap){
      vect.y -= gap;
      _pos.y -= gap;
    }else{
      _pos.y -= gap;
      vect.y = 0;
    }
  }

  //MOVING SUBTILE PER SUBTILE
  float step;
  while(vect.y > 0){
    if(vect.y < 1){
      step = vect.y;
      vect.y = 0;
    }else{
      step = 1;
      vect.y -= 1;
    }
    if(canMoveUp()){
      _pos.y -= step;
    }
  }
}
void Entity_Char::moveRight(sf::Vector2f vect){
  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if((_pos.x+_size.x) != float(int(_pos.x+_size.x))){
    float gap = 1 - ((_pos.x+_size.x) - float(int(_pos.x+_size.x)));
    if(vect.x >= gap){
      vect.x -= gap;
      _pos.x += gap;
    }else{
      _pos.x += gap;
      vect.x = 0;
    }
  }

  //MOVING SUBTILE PER SUBTILE
  bool right, stepUp, stepDown;
  float step;
  while(vect.x > 0){
    if(vect.x<1){
      step = vect.x;
      vect.x = 0;
    }else{
      step = 1;
      vect.x -= 1;
    }
    checkRightPossibilities(right, stepUp, stepDown);
    if(right){
      _pos.x += step;
    }else if(stepUp){
      _pos.y -= 1;
      _pos.x += step;
    }else if(stepDown){
      _pos.y += 1;
      _pos.x += step;
    }
  }
}
void Entity_Char::moveRightOnFloor(sf::Vector2f vect){
  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if((_pos.x+_size.x) != float(int(_pos.x+_size.x))){
    float gap = 1 - ((_pos.x+_size.x) - float(int(_pos.x+_size.x)));
    if(vect.x >= gap){
      vect.x -= gap;
      _pos.x += gap;
    }else{
      _pos.x += gap;
      vect.x = 0;
    }
  }

  //MOVING SUBTILE PER SUBTILE
  bool right, stepUp, stepDown;
  float step;
  while(vect.x > 0){
    if(vect.x<1){
      step = vect.x;
      vect.x = 0;
    }else{
      step = 1;
      vect.x -= 1;
    }
    checkRightPossibilities(right, stepUp, stepDown);
    if(stepDown){
      _pos.y += 1;
      _pos.x += step;
    }else if(right){
      _pos.x += step;
    }else if(stepUp){
      _pos.y -= 1;
      _pos.x += step;
    }
  }
}
void Entity_Char::moveLeft(sf::Vector2f vect){
  // THERE'S A STRANGE BEHAVIOR HERE :
  // WHEN MOVING RIGHT WHILE JUMPING (OR RATHER FLYING),
  //  THE CHAR SLIDES DOWN RIGHT BELLOW NEAST SLOPES.
  // BUT WHEN MOVING LEFT, IT STICKS ON INTERTILES.

  vect.x *= -1; // to simplify

  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if(_pos.x != float(int(_pos.x))){
    float gap = _pos.x - float(int(_pos.x));
    if(vect.x >= gap){
      vect.x -= gap;
      _pos.x = float(int(_pos.x));
    }else{
      _pos.x -= gap;
      vect.x = 0;
    }
  }

  //MOVING SUBTILE PER SUBTILE
  bool left, stepUp, stepDown;
  float step;
  while(vect.x > 0){
    if(vect.x<1){
      step = vect.x;
      vect.x = 0;
    }else{
      step = 1;
      vect.x -= 1;
    }
    checkLeftPossibilities(left, stepUp, stepDown);
    if(left){
      _pos.x -= step;
    }else if(stepUp){
      _pos.y -= 1;
      _pos.x -= step;
    }else if(stepDown){
      _pos.y += 1;
      _pos.x -= step;
    }
  }
}
void Entity_Char::moveLeftOnFloor(sf::Vector2f vect){
  // THERE'S A STRANGE BEHAVIOR HERE :
  // WHEN MOVING RIGHT WHILE JUMPING (OR RATHER FLYING),
  //  THE CHAR SLIDES DOWN RIGHT BELLOW NEAST SLOPES.
  // BUT WHEN MOVING LEFT, IT STICKS ON INTERTILES.

  vect.x *= -1; // to simplify

  //MOVING TO THE EDGE OF IT'S SUBTILE (scheme 4.0 & 4.1)
  if(_pos.x != float(int(_pos.x))){
    float gap = _pos.x - float(int(_pos.x));
    if(vect.x >= gap){
      vect.x -= gap;
      _pos.x = float(int(_pos.x));
    }else{
      _pos.x -= gap;
      vect.x = 0;
    }
  }

  //MOVING SUBTILE PER SUBTILE
  bool left, stepUp, stepDown;
  float step;
  while(vect.x > 0){
    if(vect.x<1){
      step = vect.x;
      vect.x = 0;
    }else{
      step = 1;
      vect.x -= 1;
    }
    checkLeftPossibilities(left, stepUp, stepDown);
    if(stepDown){
      _pos.y += 1;
      _pos.x -= step;
    }else if(left){
      _pos.x -= step;
    }else if(stepUp){
      _pos.y -= 1;
      _pos.x -= step;
    }
  }
}

void Entity_Char::setShape(float x0, float x1, float y0, float y1){
  _shape[0].position = sf::Vector2f(x0,y0);
  _shape[1].position = sf::Vector2f(x1,y0);
  _shape[2].position = sf::Vector2f(x1,-y1);
  _shape[3].position = sf::Vector2f(x0,-y1);
}
void Entity_Char::setTexZone(float x0, float x1, float y0, float y1){
  _shape[3].texCoords = sf::Vector2f(x0,y0);
  _shape[2].texCoords = sf::Vector2f(x1,y0);
  _shape[1].texCoords = sf::Vector2f(x1,y1);
  _shape[0].texCoords = sf::Vector2f(x0,y1);
}
bool Entity_Char::isTexInverted(){
  return _texInvert;
}
void Entity_Char::setTexInvertion(bool invert){
 _texInvert = invert;
}


bool Entity_Char::canMoveLeft(){
  if(_pos.x != float(int(_pos.x)))
    return true;

  bool left, nope1, nope2;
  checkLeftPossibilities(left, nope1, nope2);
  return left;
}
void Entity_Char::checkLeftPossibilities(bool & left, bool & upLeft, bool & downLeft){
  left = true;
  upLeft = true;
  downLeft = true;

  float x0, x1, y0, y1;
  int i0, i1, j0, j1;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;
  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  if(!_inst->isSubTileFree(i0-1,j0)){
    left = false;
    upLeft = false;
  }
  if(!_inst->isSubTileFree(i0-1,j1)){
    left = false;
    downLeft = false;
  }
  for(int j = j0+1 ; j <= j1-1 ; ++j){
    if(!_inst->isSubTileFree(i0-1,j)){
      left = false;
      upLeft = false;
      downLeft = false;
      break;
    }
  }
  for(int i = i0-1 ; i <= i1-1 ; ++i){
    if(!_inst->isSubTileFree(i,j0-1)){
      upLeft = false;
      break;
    }
  }
  for(int i = i0-1 ; i <= i1-1 ; ++i){
    if(!_inst->isSubTileFree(i,j1+1)){
      downLeft = false;
      break;
    }
  }
}
bool Entity_Char::canMoveRight(){
  if((_pos.x+_size.x) != float(int(_pos.x+_size.x)))
    return true;

  bool right, nope1, nope2;
  checkRightPossibilities(right, nope1, nope2);
  return right;
}
bool Entity_Char::canStepUpRight(){
  bool upRight, nope1, nope2;
  checkRightPossibilities(nope1, upRight, nope2);
  return upRight;
}
bool Entity_Char::canStepDownRight(){
  bool downRight, nope1, nope2;
  checkRightPossibilities(nope1, nope2, downRight);
  return downRight;
}
void Entity_Char::checkRightPossibilities(bool & right, bool & upRight, bool & downRight){
  right = true;
  upRight = true;
  downRight = true;

  float x0, x1, y0, y1;
  int i0, i1, j0, j1;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;
  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  if(!_inst->isSubTileFree(i1+1,j0)){
    right = false;
    upRight = false;
  }
  if(!_inst->isSubTileFree(i1+1,j1)){
    right = false;
    downRight = false;
  }
  for(int j = j0+1 ; j <= j1-1 ; ++j){
    if(!_inst->isSubTileFree(i1+1,j)){
      right = false;
      upRight = false;
      downRight = false;
      break;
    }
  }
  for(int i = i0+1 ; i <= i1+1 ; ++i){
    if(!_inst->isSubTileFree(i,j0-1)){
      upRight = false;
      break;
    }
  }
  for(int i = i0+1 ; i <= i1+1 ; ++i){
    if(!_inst->isSubTileFree(i,j1+1)){
      downRight = false;
      break;
    }
  }
}
bool Entity_Char::canMoveUp(){
  if((_pos.y-_size.y) != float(int(_pos.y-_size.y)))
    return true;

  float x0, x1, y0;
  int i0, i1, j0;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;

  for(int i = i0 ; i <= i1 ; ++i){
    if(!_inst->isSubTileFree(i,j0-1)){
      return false;
    }
  }
  return true;
}
bool Entity_Char::canMoveDown(bool extraFeet){
  //EXTRAFEET, SET TO FALSE BY DEFAULT (BECAUSE NOT NICELY FUNCTIONNING),
  // was to allow to land more easily to a platform, without having a hitbox
  // going to far away from the body.
  //It causes strange behavior, including griping walls while falling

  float extra = (extraFeet)? _feetExtraSize : 0;

  if(_pos.y != float(int(_pos.y)))
    return true;

  float x0, x1, y1;
  int i0, i1, j1;

  x0 = _pos.x - extra;
  i0 = x0;
  x1 = _pos.x + _size.x + extra;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  for(int i = i0 ; i <= i1 ; ++i){
    if(!_inst->isSubTileFree(i,j1+1)){
      return false;
    }
  }
  return true;
}

bool Entity_Char::canWallRunLow(){
  if(_texInvert){ //then oriented to the right
    float x1, y0, y1;
    int i1, j0, j1;

    x1 = _pos.x + _size.x;
    i1 = x1;
    if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
      i1 -= 1;

    y0 = _pos.y - (_size.y/2);
    j0 = y0;
    y1 = _pos.y;
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i1+1,j)){
        return true;
      }
    }
  }else{ //then oriented to the left
    float x0, y0, y1;
    int i0, j0, j1;

    x0 = _pos.x;
    i0 = x0;

    y0 = _pos.y - (_size.y/2);
    j0 = y0;
    y1 = _pos.y;
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i0-1,j)){
        return true;
      }
    }
  }
  return false;
}
bool Entity_Char::canWallRunHigh(){
  if(_texInvert){ //then oriented to the right
    float x1, y0, y1;
    int i1, j0, j1;

    x1 = _pos.x + _size.x;
    i1 = x1;
    if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
      i1 -= 1;

    y0 = _pos.y - _size.y;
    j0 = y0;
    y1 = _pos.y - (_size.y/2);
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i1+1,j)){
        return true;
      }
    }
  }else{ //then oriented to the left
    float x0, y0, y1;
    int i0, j0, j1;

    x0 = _pos.x;
    i0 = x0;

    y0 = _pos.y - _size.y;
    j0 = y0;
    y1 = _pos.y - (_size.y/2);
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i0-1,j)){
        return true;
      }
    }
  }
  return false;
}
bool Entity_Char::canWallGrab(){
  int i,j;
  return canWallGrab(i, j);
}
bool Entity_Char::canWallGrab(int & tile_i, int & tile_j){
  // will try to find a grab from just above the top to the middle of the body

  if(_texInvert){ //then oriented to the right
    float x1, y0, y1;
    int i1, j0, j1;

    x1 = _pos.x + _size.x;
    i1 = x1;
    if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
      i1 -= 1;

    y0 = _pos.y - _size.y;
    j0 = y0;
    y1 = _pos.y - (_size.y/2);
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i1+1,j) and _inst->isSubTileFree(i1+1,j-1)){
        tile_i = i1+1;
        tile_j = j;
        return true;
      }
    }
  }else{ //then oriented to the left
    float x0, y0, y1;
    int i0, j0, j1;

    x0 = _pos.x;
    i0 = x0;

    y0 = _pos.y - _size.y;
    j0 = y0;
    y1 = _pos.y - (_size.y/2);
    j1 = y1;
    if( y1 == float(int(y1)) ){
      j1 -= 1;
    }

    for(int j = j0+1 ; j <= j1-1 ; ++j){
      if(!_inst->isSubTileFree(i0-1,j) and _inst->isSubTileFree(i0-1,j-1)){
        tile_i = i0-1;
        tile_j = j;
        return true;
      }
    }
  }

  return false;
}

void Entity_Char::setJumpSpeed(float speed){
  _jumpSpeed = speed;
}
void Entity_Char::setBaseSpeed(float speed){
  _baseSpeed = speed;
}

float Entity_Char::getBaseSpeed(){
  return _baseSpeed;
}
sf::Vector2f Entity_Char::getSpeed(){
  return _speed;
}

void Entity_Char::setHoriSpeed(float speed){
  _speed.x = speed;
}
void Entity_Char::setVertSpeed(float speed){
  _speed.y = speed;
}

void Entity_Char::moveHoriSpeed(float speed){
  _speed.x += speed;
}
void Entity_Char::moveVertSpeed(float speed){
  _speed.y += speed;
}


/*
How to make a good collision detection (my guesses ):
The way I make it currently is collision on surfaces and not on volumes,
 which is very efficient.
The next step is to decompose the move. First it sees the closest collision detected,
 then it watches the possible move remaining and redo a collision research.
*/

// https://en.sfml-dev.org/forums/index.php?topic=1393.0
/*
usually you shouldn't compare to a specific value (ie: if(velocity==0.f)),
 instead you should use some epsilon value depending on the context,
 because it's not fixed-point, maybe if you're handling houndreds,
 epsilon could be 0.1f, but if you're handling microns, it should be 0.0000001.

You should know that floats have 6 digit significant value.

1000000.f + 1.f = 1000000.f
*/

bool Entity_Char::AABBCollision(sf::Vector2f bPos, sf::Vector2f bSize, sf::Vector2f & shortVect, sf::Vector2f & normale)
{
  //what helped me : http://zaunfish.blogspot.com/2018/03/c-and-sfml-tile-based-collision.html
  //another interesting discussion : https://en.sfml-dev.org/forums/index.php?topic=18289.0

  // body parts shortcuts

  float left0 = _pos0.x;
  float right0 = _pos0.x + _size.x;
  float above0 = _pos0.y - _size.y;
  float below0 = _pos0.y;

  float left = _vect1.x + _pos0.x;
  float right = _vect1.x + _pos0.x + _size.x;
  float above = _vect1.y + _pos0.y - _size.y;
  float below = _vect1.y + _pos0.y;

  float bLeft = bPos.x;
  float bRight = bPos.x + bSize.x;
  float bAbove = bPos.y;
  float bBelow = bPos.y + bSize.y;

  bool hitAbove = false;
  bool hitBelow = false;
  bool hitRight = false;
  bool hitLeft = false;
  float ratio;

  if(_vect1.x == 0){ //vertical movement (ex: jumping or falling)

    if( (right0 > bLeft) and (left0 < bRight) ){
      if(_vect1.y > 0){ // falling

        if( (below0 <= bAbove) and (below > bAbove)){
          hitBelow = true;
        }
      }else{ // jumping

        if( (above0 >= bBelow) and (above < bBelow) ){
          hitAbove = true;
        }
      }
    }
    else return false;
  }
  else if(_vect1.y == 0){ //horizontal movement (walking, running, planning...)

    if( (below > bAbove) and (above < bBelow) ){
      if(_vect1.x < 0){ // going left

        if( (left0 >= bRight) and (left < bRight)){
          hitLeft = true;
        }
      }else{ // going right

        if( (right0 <= bLeft) and (right > bLeft) ){
          hitRight = true;
        }
      }
    }
    else return false;
  }
  else{ //wonderful world of diagonal movements
    bool isFarAbove = (below0 < bAbove) and (below < bAbove);
    bool isFarBelow = (above0 > bBelow) and (above > bBelow);
    bool isFarLeft  = (right0 < bLeft) and (right < bLeft);
    bool isFarRight = (left0 > bRight) and (left > bRight);

    if(isFarRight or isFarLeft or isFarAbove or isFarBelow)
      return false;

    sf::Vector2f lowerLeft0 = _pos0;
    sf::Vector2f lowerRight0 = _pos0 + sf::Vector2f(_size.x,0);
    sf::Vector2f upperLeft0 = _pos0 + sf::Vector2f(0,-_size.y);
    sf::Vector2f upperRight0 = _pos0 + sf::Vector2f(_size.x,-_size.y);

    sf::Vector2f bLowerLeft = bPos + sf::Vector2f(0,bSize.y);
    sf::Vector2f bLowerRight = bPos + bSize;
    sf::Vector2f bUpperLeft = bPos;
    sf::Vector2f bUpperRight = bPos + sf::Vector2f(bSize.x,0);

    if( (_vect1.x > 0) and (_vect1.y > 0) ){ //going down right

      sf::Vector2f dist = bUpperLeft - lowerRight0; // distance between the two interesting corners of the objets

      if(dist.x < 0){ //might only hit above/below
        sf::Vector2f dist2 = bUpperRight - lowerLeft0;

        if( (_vect1.x / _vect1.y) < (dist2.x / dist2.y) ){ //else, it misses the box
          if(_vect1.y > dist2.y){ //then it hits
            hitBelow = true;
          }
        }
      }else if(dist.y < 0){ //might only hit right/left
        sf::Vector2f dist2 = bLowerLeft - upperRight0;

        if( (_vect1.y / _vect1.x) < (dist2.y / dist2.x) ){ //else, it misses the box
          if(_vect1.x > dist2.x){ //then it hits
            hitRight = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y > dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it land above the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // then the player would like to LAND rather than to FALL
            hitBelow = true;
          }else{
            hitRight = true;
          }
        }
      }

    }else if( (_vect1.x > 0) and (_vect1.y < 0) ){
      //going up right

      sf::Vector2f dist = bLowerLeft - upperRight0; // distance between the two interesting corners of the objets

      if(dist.x < 0){ //might only hit above/below
        sf::Vector2f dist2 = bLowerRight - upperLeft0;

        if( (dist2.y == 0) or ((_vect1.x / _vect1.y) > (dist2.x / dist2.y)) ){ //else, it misses the box (y values are inverted)
          if(_vect1.y < dist2.y){ //then it hits (y values inverted)
            hitAbove = true;
          }
        }
      }else if(dist.y > 0){ //might only hit right/left (y values inverted)
        sf::Vector2f dist2 = bUpperLeft - lowerRight0;

        if( (_vect1.y / _vect1.x) > (dist2.y / dist2.x) ){ //else, it misses the box (y values inverted)
          if(_vect1.x > dist2.x){ //then it hits
            hitRight = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y < dist.y) ){ //it hits (y values inverted)
          if( (_vect1.x / _vect1.y) <= (dist.x / dist.y) ){ //case where it stays below the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // would the player prefer to pass up or right ? I'd say right
            hitAbove = true;
          }else{
            hitRight = true;
          }
        }
      }
    }else if( (_vect1.x < 0) and (_vect1.y > 0) ){ //going down left

      sf::Vector2f dist = bUpperRight - lowerLeft0; // distance between the two interesting corners of the objets

      if(dist.x > 0){ //might only hit above/below (x values are inverted)
        sf::Vector2f dist2 = bUpperLeft - lowerRight0;

        if( (_vect1.x / _vect1.y) > (dist2.x / dist2.y) ){ //else, it misses the box (x values are inverted)
          if(_vect1.y > dist2.y){ //then it hits
            hitBelow = true;
          }
        }
      }else if(dist.y < 0){ //might only hit left/right
        sf::Vector2f dist2 = bLowerRight - upperLeft0;

        if( (_vect1.x / _vect1.y) < (dist2.x / dist2.y) ){ //else, it misses the box (x values are inverted)
          if(_vect1.x < dist2.x){ //then it hits
            hitLeft = true;
          }
        }
      }else{
        if( (_vect1.x < dist.x) and (_vect1.y > dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it land above the box //THAT PART BUGS ME
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // then the player would like to LAND rather than to FALL
            hitBelow = true;
          }else{
            hitLeft = true;
          }
        }
      }
    }else if( (_vect1.x < 0) and (_vect1.y < 0) ){
      //going up left

      sf::Vector2f dist = bLowerRight - upperLeft0; // distance between the two interesting corners of the objets

      if(dist.x > 0){ //might only hit above/below (x values inverted)
        sf::Vector2f dist2 = bLowerLeft - upperRight0;

        if( (dist2.y == 0) or ((_vect1.x / _vect1.y) < (dist2.x / dist2.y)) ){ //else, it misses the box
          if(_vect1.y < dist2.y){ //then it hits (y values inverted)
            hitAbove = true;
          }
        }
      }else if(dist.y > 0){ //might only hit right/left (y values inverted)
        sf::Vector2f dist2 = bUpperRight - lowerLeft0;

        if( (_vect1.x / _vect1.y) > (dist2.x / dist2.y) ){ //else, it misses the box
          if(_vect1.x < dist2.x){ //then it hits (x values inverted)
            hitLeft = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y < dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it stays below the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // would the player prefer to pass up or left ? I'd say left
            hitAbove = true;
          }else{
            hitLeft = true;
          }
        }
      }
    }
  }

  if(hitAbove){
    shortVect.y = bBelow - above0;
    ratio = shortVect.y / _vect1.y;
    shortVect.x = _vect1.x * ratio;

    normale.x = 0;
    normale.y = 1;

    return true;
  }else if(hitBelow){
    shortVect.y = bAbove - below0;
    ratio = shortVect.y / _vect1.y;
    shortVect.x = _vect1.x * ratio;

    normale.x = 0;
    normale.y = -1;

    return true;
  }else if(hitRight){
    shortVect.x = bLeft - right0;
    ratio = shortVect.x / _vect1.x;
    shortVect.y = _vect1.y * ratio;

    normale.x = -1;
    normale.y = 0;

    return true;
  }else if(hitLeft){
    shortVect.x = bRight - left0;
    ratio = shortVect.x / _vect1.x;
    shortVect.y = _vect1.y * ratio;

    normale.x = 1;
    normale.y = 0;

    return true;
  }

  return false;
}

void Entity_Char::play(sf::Event & event, sf::Time _turnTime){

  //#####################
  // Initial Situation :
  // - If keys have been pressed, Command methods have already been used
  // - There must be a flag system,
  // so that going NEast isn't just a sum of going North and going East
  // - There might be a hierarchy in commands.
  // Direction commands are grouped, but what about jumping, sprinting and hitting ?
  // - There must be a state system.
  // Jumping while in ground or while in air is QUITE different.


  //loadListOfCommands(event);
  // flags needed :
  // - what key is already holding
  // - what action is in course
  // - state (falling)
  // types of commands :
  // - Cooldown vs Nonstop
  // - Press-Release vs Spammable

  //prise en compte des actions types : course, marche, saut, attaques...

  //gestion des déplacements et collisions avec les autres entités
  //envoie des effets aux entités concernés, pour la partie update
  //si immobile, écartement des entités empilés (magnétisme)

  /*float delta = 100 * _turnTime.asSeconds() * -1;
    _shape.move(0,delta);
  float delta = 100 * _turnTime.asSeconds();
    _shape.move(0,delta);
  float delta = 100 * _turnTime.asSeconds() * -1;

    _shape.move(delta,0);
  float delta = 100 * _turnTime.asSeconds();
    _shape.move(delta,0);*/
}

void Entity_Char::update(sf::Time turnTime){
  _turnTime = turnTime;

  _state->update();

  // DON'T LIKE IT, GOTTA FIND SOMETHING IN WHICH PUT THAT BLOCK
  if(!_leftClic0 and _leftClic){
    sf::Vector2f mouse = sf::Vector2f(_inst->getMousePos());
    sf::Vector2f offset = mouse - _inst->getScreenCenter();
    sf::Vector2f pos = _pos + (offset / _inst->getZoom());

    int x = int(pos.x);
    int y = int(pos.y);
    int id = _inst->getWorld()->getIdAt(x/16,y/16); //gotta make it cleaner
    if(id == 255){
      _addingTiles = true;

      _inst->getWorld()->setIdAt(_selectId,x,y);
      _inst->getWorld()->updateTypesAt(x,y);
      _inst->getWorld()->updateVerticesAt(x,y);
    }else{
      _addingTiles = false;

      _inst->getWorld()->setIdAt(255,x,y);
      _inst->getWorld()->updateTypesAt(x,y);
      _inst->getWorld()->updateVerticesAt(x,y);
    }
  }else if(_leftClic0 and _leftClic){
    sf::Vector2f mouse = sf::Vector2f(_inst->getMousePos());
    sf::Vector2f offset = mouse - _inst->getScreenCenter();
    sf::Vector2f pos = _pos + (offset / _inst->getZoom());

    int x = int(pos.x);
    int y = int(pos.y);
    if(_addingTiles){
      _inst->getWorld()->setIdAt(_selectId,x,y);
      _inst->getWorld()->updateTypesAt(x,y);
      _inst->getWorld()->updateVerticesAt(x,y);
    }else{
      _inst->getWorld()->setIdAt(255,x,y);
      _inst->getWorld()->updateTypesAt(x,y);
      _inst->getWorld()->updateVerticesAt(x,y);
    }
  }
  _leftClic0 = _leftClic;
  _leftClic = false;
}

void Entity_Char::draw(sf::RenderTarget & target, sf::RenderStates states) const{
  if(_inst->getFocused() == this){
    states.transform.translate(_inst->getScreenCenter());
    states.transform.scale(_inst->getZoom(),_inst->getZoom());
    states.texture = &_tex;

    target.draw(_shape, states);
  }else{
    sf::Vector2f focus = _inst->getFocused()->getPos();
  	sf::Vector2f recenter = _inst->getScreenCenter() - ((focus-_pos) * _inst->getZoom());

  	states.texture = &_tex;
  	states.transform.translate(recenter);
    states.transform.scale(_inst->getZoom(),_inst->getZoom());

    target.draw(_shape, states);
  }

}
