#include "Application.hpp"

// There should be only one constructor for the sake of the init function
Application::Application(int x, int y, std::string name) :
_width(x), _heigh(y), _name(name),
_window(sf::VideoMode(x, y), name)
{

}

void Application::setSize(int x, int y){
  _width = x;
  _heigh = y;
}
void Application::setName(std::string name){
  _name = name;
}
void Application::setFrameRate(int rate){
  _frameRate = rate;
}

// initialization of the window
// creation of the desired instance
// setting of the created instance's components
void Application::init(){
  //Création de la fenêtre
  //_window = sf::RenderWindow(sf::VideoMode(_width, _heigh), _name);

	_window.setFramerateLimit(_frameRate); // limit to 60FPS max (or 120)

  // Game declaration
  //_inst = new Instance(&_window);
  //_inst = new Instance_Test(&_window,sf::Vector2f(_width,_heigh));
  //_inst = new Instance_Game(&_window,sf::Vector2f(_width,_heigh));
  _inst = new Instance_Units(&_window,sf::Vector2f(_width,_heigh));

	//_inst->SetWorld();



}

// Window loop, shouldn't be modified for anything, excepts for the random things that is still incomplete
void Application::run(){
  /* initialize random seed: */
	//srand (time(NULL));

	//random_device rd{};
	//mt19937 gen{rd()};

	//normal_distribution<> d{0,-500};
  _globalTime.restart();

	//Démarrage de la boucle principale
	while (_window.isOpen())
	{
		_elapsedTime = _globalTime.restart();

		sf::Event event;
		while (_window.pollEvent(event))
		{
			//fermeture
			if (event.type == sf::Event::Closed)
				_window.close();

			//mouvement de la souris
			if (event.type == sf::Event::MouseMoved){
				sf::Vector2i mouse = sf::Mouse::getPosition(_window);
				int deltaX = mouse.x;// - int(ent_pos0.x);
				int deltaY = mouse.y;// - int(ent_pos0.y);
			}
		}
		_inst->update(_window, event, _elapsedTime);

		//affichages

		_window.clear(sf::Color(50, 50, 50));
		_inst->draw(_window);
		_window.display();

	}
}

void::Application::close(){}
