#include "SFML/Graphics.hpp"	//RenderWindow
#include "SFML/Window.hpp"		//Window - VideoMode - Style
#include "SFML/System.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <random>

float vecToRad(int deltaX, int deltaY){
	
	float cos = deltaX / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	float sin = deltaY / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	
	if(sin > 0){
		return 3.14159/2 + acos(cos);
	}else{
		return 3.14159/2 - acos(cos);
	}
}
float vecToDeg(int deltaX, int deltaY){
	
	float cos = deltaX / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	float sin = deltaY / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	
	if(sin > 0){
		return 90 + acos(cos)*180/3.14159;
	}else{
		return 90 - acos(cos)*180/3.14159;
	}
}
sf::Vector2f radToVec(float rad){
	float cosin = cos(-3.14159/2 + rad);
	float sinus = sin(-3.14159/2 + rad);
	
	return sf::Vector2f(cosin, sinus);
}
sf::Vector2f degToVec(float deg){
	float cosin = cos((deg-90)*3.14159/180);
	float sinus = sin((deg-90)*3.14159/180);
	
	return sf::Vector2f(cosin, sinus);
}
