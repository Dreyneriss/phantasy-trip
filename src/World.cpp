#include "World.hpp"

World::World(){}
World::World(sf::RenderWindow * window) : _window(window){}
//~World(); //TODO

void World::setInst(Instance_Units * inst){_inst = inst;}

void World::LoadMapFromPPM(std::string adress)
{
	// Load the ppm file into _map_ids, nothing more

	std::ifstream file(adress); //1543 800

	if(file)
	{
		std::string line;

		int i = 0; //num of the picked line
		int cell = 0; //num of the corresponding cell of the map
		int x=0, y=0; //corresponding coordinates
		int num=0, id = 0;

		while( std::getline(file, line) )
        {
			if( i == 2 ){ //header with sizes
				std::istringstream iss(line);

				if(!(iss >> _width >> _heigh)) { break;} //error

				_nbCells = _width*_heigh;

				_map_ids = std::vector<std::vector<int>>(_width);
				_map_tiles = std::vector<std::vector<int>>(_width);
				for(int l = 0; l < _width; ++l){
					_map_ids[l] = std::vector<int>(_heigh);
					_map_tiles[l] = std::vector<int>(_heigh);
				}
			}
			else if( i > 3){ //beyond header
				int l = i-4;
				if(l%3 < 2){
					std::istringstream iss(line);
					if(!(iss >> num)) { break;}
					id += num;
				}else{
					id += num;
					if(id==0)
						_map_ids[x][y] = 0;
					else if(id==257) //255+1+1
						_map_ids[x][y] = 1;
					else
						_map_ids[x][y] = 255;

					++cell;
					x = cell % _width;
					y = cell / _width;

					id = 0;
				}

			}

			++i;
		}

		file.close();
	}
	else
		std::cout << "Erreur lors du chargement du fichier "<< adress << std::endl;//erreur
}

void World::computeTilesShapes()
{
	for(int c = 0; c < _nbCells; ++c){
		int i = c % _width;
		int j = c / _width;

		int type = 0;
		if(j > 0 and _map_ids[i][j-1] == 255) // air above
			type += 1;
		if(j < _heigh-1 and _map_ids[i][j+1] == 255) // air bellow
			type += 2;
		if(i > 0 and _map_ids[i-1][j] == 255) // air on the left
			type += 4;
		if(i < _width-1 and _map_ids[i+1][j] == 255) // air on the right
			type += 8;
		_map_tiles[i][j] = type;
	}
}

void World::addTerrain(std::string name, int id, std::string adress)
{
  _terrains.push_back(Terrain(this));

	_terrains.back().SetTexture(adress);
	_terrains.back().SetName(name);
	_terrains.back().SetId(id);

	_terrains.back().SetVertices(_width, _heigh);
}

void World::SetVertices()
{

}

//##################################
// TERRAFORMATION

void World::setIdAt(int id, int x, int y){
	int i = x / _inst->getUnitPerTile(); //gotta fix it ; it's not World's job to adjust it
	int j = y / _inst->getUnitPerTile();
	if( (i >= 0) and (i < _map_ids.size()) and (j >= 0) and (j < _map_ids[0].size()) ){
		_map_ids[i][j] = id;
	}
}
void World::updateTypesAt(int x, int y){
	int tileX = x / _inst->getUnitPerTile(); //gotta fix it ; it's not World's job to adjust it
	int tileY = y / _inst->getUnitPerTile();
	int i0 = (tileX > 0)? tileX-1 : tileX;
	int i1 = (tileX < _width-1)? tileX+1 : tileX;
	int j0 = (tileY > 0)? tileY-1 : tileY;
	int j1 = (tileY < _heigh-1)? tileY+1 : tileY;
	for(int i=i0; i<=i1; ++i){
		for(int j=j0; j<=j1; ++j){
			int type = 0;
			if(j > 0 and _map_ids[i][j-1] == 255) // air above
				type += 1;
			if(j < _heigh-1 and _map_ids[i][j+1] == 255) // air bellow
				type += 2;
			if(i > 0 and _map_ids[i-1][j] == 255) // air on the left
				type += 4;
			if(i < _width-1 and _map_ids[i+1][j] == 255) // air on the right
				type += 8;
			_map_tiles[i][j] = type;
			_terrains[0].updateVerticesAt(i,j);
			_terrains[1].updateVerticesAt(i,j);
		}
	}
	/*int type = 0;
	if(tileY > 0 and _map_ids[tileX][tileY-1] == 255) // air above
		type += 1;
	if(tileY < _heigh-1 and _map_ids[tileX][tileY+1] == 255) // air bellow
		type += 2;
	if(tileX > 0 and _map_ids[tileX-1][tileY] == 255) // air on the left
		type += 4;
	if(tileX < _width-1 and _map_ids[tileX+1][tileY] == 255) // air on the right
		type += 8;
	_map_tiles[tileX][tileY] = type;
	_terrains[0].updateVerticesAt(tileX,tileY);
	_terrains[1].updateVerticesAt(tileX,tileY);*/
}
void World::updateVerticesAt(int x, int y){
	int i = x / _inst->getUnitPerTile(); //gotta fix it ; it's not World's job to adjust it
	int j = y / _inst->getUnitPerTile();
	if( (i >= 0) and (i < _map_ids.size()) and (j >= 0) and (j < _map_ids[0].size()) ){
		_terrains[0].updateVerticesAt(i,j);
		_terrains[1].updateVerticesAt(i,j);
	}
}

//###################################
// GETTERS

int World::getIdAt(int i, int j){
	if( (i>=0) and (i<_map_ids.size()) and (j>=0) and (j<_map_ids[0].size()) ){
		return _map_ids[i][j];
	}else{
		return 255; //void id
	}

}
int World::getTypeAt(int i, int j){
	if( (i>=0) and (i<_map_ids.size()) and (j>=0) and (j<_map_ids[0].size()) ){
		return _map_tiles[i][j];
	}else{
		return -1;
	}
}
int World::getPixelPerTile(){return _inst->getPixelPerTile();}
int World::getUnitPerTile(){return _inst->getUnitPerTile();}

//##################################
//

bool World::isSubTileFree(int i, int j){
	int tileX, tileY, subTileX, subTileY;

	int unitPerTile = _inst->getUnitPerTile();

	tileX = i / unitPerTile;
	subTileX = i % unitPerTile;
	tileY = j / unitPerTile;
	subTileY = j % unitPerTile;

	if(_map_ids[tileX][tileY] < 255){
		switch(_map_tiles[tileX][tileY]){
			case 10 : { // above & left : NWest triangle
				/*
				*****
				****-
				***--
				**---
				*----
				*/

				if( (subTileX + subTileY) > unitPerTile )
					return true;
				else
					return false;

				break;
			}
			case 9 : { // bellow & left : SWest triangle
				/*
				*----
				**---
				***--
				****-
				*****
				*/

				if( subTileX > subTileY )
					return true;
				else
					return false;

				break;
			}
			case 6 : { // above & right : NEast triangle
				/*
				*****
				-****
				--***
				---**
				----*
				*/

				if( subTileY > subTileX )
					return true;
				else
					return false;

				break;
			}
			case 5 : { // bellow & right : SEast triangle
				/*
				----*
				---**
				--***
				-****
				*****
				*/

				if( (subTileX + subTileY) < unitPerTile )
					return true;
				else
					return false;

				break;
			}
			default : { // normal case
				return false;

				break;
			}
		}
	}else{
		return true;
	}
}

sf::Vector2f World::getScreenCenter(){return _inst->getScreenCenter();}
float World::getZoom(){return _inst->getZoom();}
sf::Vector2f World::getFocusedPos(){
	Entity * focused = _inst->getFocused();
	if(focused != NULL)
		return focused->getPos();
	else
		return sf::Vector2f(0,0);
}

void World::draw(sf::RenderWindow & window){
	sf::RenderStates states;

	window.draw(_terrains[0]);
	window.draw(_terrains[1]);
}
