#include "Instance_Units.hpp"
#include "Command.hpp"
#include "InputHandler.hpp"


//#################
// Constructor

Instance_Units::Instance_Units(sf::RenderWindow * window, sf::Vector2f screenSize) : Instance(window, screenSize)
{
  _inputHandler.initDefault();
  setScreenCenter(0.5,0.6);

  //##################
  // World definition

  _unitPerTile = 16;
  _pixelPerUnit = 2; // may disapear, caus' of the possibility of zooming and unzooming
  _pixelPerTile = _unitPerTile * _pixelPerUnit;
  _tileStandard = 3;
  _meterStandard = 1;
  _meterPerTile = float(_tileStandard)/float(_meterStandard);
  _tilePerMeter = float(_meterStandard)/float(_tileStandard);
  //_zoom = _pixelPerTile;
  _zoom = _pixelPerUnit;

  _world = new World();
  _world->setInst(this);
  _world->LoadMapFromPPM("perlin_map55.ppm");
  _world->computeTilesShapes();
  _world->addTerrain("Ground",0, "data/sprites/ground.png"); // name, id
  _world->addTerrain("Marble",1, "data/sprites/marble.png");

  _gravAcc = 1000; // units/s²
  //_focus->setJumpSpeed(-80);
  //_focus->setBaseSpeed(30);
  _gravMax = 2000; // pix/s ; Later, distance unit should be tile or meter, not pixel

  //#####################
  // Characters creation

  //character's size must not be round, so that it doesn't struggle with likely sized holes
  float charX, charY;
  //sf::Vector2i charPos(1370,230);
  //sf::Vector2i charPos(1213,590);
  //sf::Vector2i charPos(1020,485);
  //sf::Vector2i charPos(272,317);
  //sf::Vector2i charPos(525,620);
  //sf::Vector2i charPos(535,210);
  sf::Vector2i charPos(500,595);
  charX = charPos.x * _unitPerTile;
  charY = charPos.y * _unitPerTile;
  addCharacter(charX,charY);
  _focus = _char.back();
  addCharacter(charX-32,charY);
}

//#################
// Creation

void Instance_Units::addWorldUnit(int width, int heigh){
  //_world.push_back(new Entity_Char2(width, heigh, this));
}

void Instance_Units::addCharacter(float x, float y){
  _char.push_back(new Entity_Char(x, y, this));
}

World * Instance_Units::getWorld(){
  return _world;
}

void Instance_Units::switchFocused(){
  if(_char.size()>1){
    int i;
    for(i=0; i<_char.size(); ++i){
      if(_char[i] == _focus){
        break;
      }
    }
    i = (i+1)%_char.size();
    _focus = _char[i];
  }
}

//########################
// Physics


void Instance_Units::applyGravity(Entity_Char* ent, sf::Time _turnTime){
  float currSpeed = ent->getSpeed().y;

  if(currSpeed < _gravMax){ //if entity hasn't already it's max speed
    float acc = _gravAcc*_turnTime.asSeconds();

    if((currSpeed+acc) > _gravMax) //to not go beyond max speed
      ent->setVertSpeed(_gravMax);
    else
      ent->moveVertSpeed(acc);
  }
}

bool Instance_Units::isSubTileFree(int i, int j){
  return _world->isSubTileFree(i, j);
}

//###########################
// Instance common methods

void Instance_Units::setScreenCenter(float ratioX, float ratioY){
  _screenCenter.x = _screenSize.x*ratioX;
  _screenCenter.y = _screenSize.y*ratioY;
}

void Instance_Units::update(sf::RenderWindow & window, sf::Event & event, sf::Time _turnTime)
{
  // taking commands, managing collision ?

  _inputHandler.handleInput(_focus);
  //mouvement de la souris
	if (event.type == sf::Event::MouseMoved){
		_mouse = sf::Mouse::getPosition(window);
	}

  // Gravity on falling objects
  for(int i = 0 ; i < _char.size() ; ++i)
  {
    if(_char[i]->canMoveDown()){
      applyGravity(_char[i], _turnTime);
    }
  }

  //
  for(int i = 0 ; i < _char.size() ; ++i)
  {
    _char[i]->play(event, _turnTime);
  }

  // managing states
  for(int i = 0 ; i < _char.size() ; ++i)
  {
    _char[i]->update(_turnTime);
  }
}

void Instance_Units::draw(sf::RenderWindow & window){
  _world->draw(window);
  for(int i = 0 ; i < _char.size() ; ++i)
  {
    window.draw(*_char[i]);
  }

  /*for(int i = 0 ; i < _world.size() ; ++i)
  {
    window.draw(*_world[i]);
  }*/
}
