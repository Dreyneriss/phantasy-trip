#include "SFML/Graphics.hpp"	//RenderWindow
#include "SFML/Window.hpp"		//Window - VideoMode - Style
#include "SFML/System.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <random>

#include "Application.hpp"

unsigned int BOX_ID = 0;

using namespace std;

int main(){

	Application app(1280, 720, "Game");

	app.setFrameRate(120);

	app.init();
	app.run();
	app.close(); //useless right now

	return 0;
}
