#include "Dynamic_State.hpp"

AirJumping_State::AirJumping_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  _limit = sf::seconds(0.5f);
  _cooldown = sf::seconds(0.5f);

  _spriteCycle = std::vector<std::vector<float>>(2);
  _spriteCycle[0] = std::vector<float>{0.f};
  _spriteCycle[1] = std::vector<float>{.03f, .06f, .1f, .14f, .19f, .25f, .32f, .4f};
  //_spriteCycle[1] = std::vector<float>{.03f, .07f, .12, .18f, .25, .33, .42, .5};
  //_spriteCycle[1] = std::vector<float>{.06f, .12f, .18f, .24f, .3f, .36f, .42f, .48f};
  //_spriteCycle[1] = std::vector<float>{.05f, .1f, .15, .2f, .25, .3, .35, .4};
  //_spriteCycle[1] = std::vector<float>{.075f, .15f, .225f, .3f, .375f, .45f, .525f, .7f};
  //_spriteCycle[1] = std::vector<float>{.1f, .2f, .3f, .4f, .5f, .6f, .7f, .8f};

  _texCoords = std::vector<std::vector<std::vector<float>>>(2);
  _texCoords[0] = std::vector<std::vector<float>>(1);
  _texCoords[0][0] = std::vector<float>{ 85, 332, 2875, 3786, -3.5, 8 };
  _texCoords[1] = std::vector<std::vector<float>>(8);
  _texCoords[1][0] = std::vector<float>{ 108, 300, 6230, 7132, 0, 8 };
  _texCoords[1][1] = std::vector<float>{ 408, 626, 6230, 7132, 0, 8 };
  _texCoords[1][2] = std::vector<float>{ 659, 1017, 6230, 7132, -9, 8 };
  _texCoords[1][3] = std::vector<float>{ 1190, 1424, 6230, 7132, -3.5, 8 };
  _texCoords[1][4] = std::vector<float>{ 1532, 1724, 6230, 7132, -1, 8 };
  _texCoords[1][5] = std::vector<float>{ 1823, 2075, 6230, 7132, -4.5, 8 };
  _texCoords[1][6] = std::vector<float>{ 2253, 2610, 6230, 7132, -9.5, 8 };
  _texCoords[1][7] = std::vector<float>{ 2805, 3057, 6230, 7132, -3, 8 };
}

//##########################
// COMMANDS

void AirJumping_State::c_jump(){
  _jumping = true;
}

//###########################
//

sf::Time AirJumping_State::getTime(){
  _time -= _clock.restart();
  return _time;
}

void AirJumping_State::init(){
  _clock.restart();
  _time = sf::Time::Zero;
  _jumping = true;

  _char->setVertSpeed(_jumpSpeed);

  setSprite(1,0);
}

void AirJumping_State::update(){
  if(_jumping){
    _time += _clock.restart();
    if(_time <= _limit){
      int sprite = _spriteCycle[1].size()-1;
      for(int i=0; i<_spriteCycle[1].size(); ++i){
        if(_time.asSeconds() < _spriteCycle[1][i]){
          sprite = i;
          break;
        }
      }
      setSprite(1,sprite);
      if(_char != NULL){
        _char->setVertSpeed(_jumpSpeed);
      }
    }else{
      _time = _cooldown;
      _clock.restart();
      _char->setState(FALLING_STATE);
    }
  }else{
    _time = _cooldown;
    _clock.restart();
    _char->setState(FALLING_STATE);
  }
  _jumping = false;

  _char->move();
  _char->setHoriSpeed(0);
  if(!_char->canMoveUp() and _char->getSpeed().y<0)
    _char->setVertSpeed(0);
}
