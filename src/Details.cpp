#include "Details.hpp"

Details::Details(std::vector<std::vector<int>> & map_id, std::vector<std::vector<int>> & map_cond, int & width, int & heigh, int & nbcell, sf::Vector2f & pos) : _map_id(map_id), _map_cond(map_cond), _width(width), _heigh(heigh), _nbCells(nbcell), _position(pos)
{_tilesPerPannel = sf::Vector2i(100,100);}
//~Details(); //TODO


void Details::move(int x, int y)
{
  //_position.x += x;
  //_position.y += y;
}


void Details::SetTexture(std::string adress)
{
	_tex.loadFromFile(adress);
}
void Details::SetName(std::string name)
{
	_name = name;
}
void Details::SetId(int id)
{
	_id = id;
}

/*void Details::SetVertices()
{
	int i, j; //coordinates of cells
	float x, y; //coordinates of quads
	
	//double sprite, one centered on cells, one centered on cells' joints
	int nbJoints = (_width -1)*(_heigh -1);
	
	_vertices = sf::VertexArray(sf::Quads, 4*(_nbCells+nbJoints));
	for(int d = 0 ; d < _nbCells ; ++d){
		// borders
		i = d % _width;
		j = d / _width;
		
		x = i*16;
		y = j*16;
		
		if(_map_id[i][j] != 255){
		
			int condition = _map_cond[i][j];
			
			if(condition != 0){
				_vertices[4*d].position = sf::Vector2f(x,y);
				_vertices[4*d +1].position = sf::Vector2f(x+16,y);
				_vertices[4*d +2].position = sf::Vector2f(x+16,y+16);
				_vertices[4*d +3].position = sf::Vector2f(x,y+16);
			}
			
			switch(condition)
			{
				case 1 : { // only above
					_vertices[4*d].texCoords = sf::Vector2f(0.f,0.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(16.f,0.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(0.f,16.f);
					
					break;
				}
				case 2 : { // only bellow
					_vertices[4*d].texCoords = sf::Vector2f(0.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(16.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(0.f,32.f);
					
					break;
				}
				case 3 : { // above & bellow
					_vertices[4*d].texCoords = sf::Vector2f(64.f,0.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(80.f,0.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(80.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(64.f,16.f);
					
					break;
				}
				case 4 : { // only left
					_vertices[4*d].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(32.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(16.f,32.f);
					
					break;
				}
				case 5 : { // above & left
					_vertices[4*d].texCoords = sf::Vector2f(32.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(48.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(32.f,16.f);
					
					break;
				}
				case 6 : { // bellow & left
					_vertices[4*d].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(64.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(48.f,32.f);
					
					break;
				}
				case 7 : { // above, bellow & left
					_vertices[4*d].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(112.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(112.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(96.f,32.f);
					
					break;
				}
				case 8 : { // only right
					_vertices[4*d].texCoords = sf::Vector2f(16.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(32.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(16.f,16.f);
					
					break;
				}
				case 9 : { // above & right
					_vertices[4*d].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(48.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(32.f,32.f);
					
					break;
				}
				case 10 : { // bellow & right
					_vertices[4*d].texCoords = sf::Vector2f(48.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(64.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(48.f,16.f);
					
					break;
				}
				case 11 : { // above, bellow & right 
					_vertices[4*d].texCoords = sf::Vector2f(96.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(112.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(112.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(96.f,16.f);
					
					break;
				}
				case 12 : { // left & right
					_vertices[4*d].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(80.f,32.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(80.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(64.f,16.f);
					
					break;
				}
				case 13 : { // above, left & right
					_vertices[4*d].texCoords = sf::Vector2f(80.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(96.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(80.f,16.f);
					
					break;
				}
				case 14 : { // bellow, left & right
					_vertices[4*d].texCoords = sf::Vector2f(80.f,16.f);
					_vertices[4*d +1].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[4*d +2].texCoords = sf::Vector2f(96.f,32.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(80.f,32.f);
					
					break;
				}
				case 16 : { // all directions
					_vertices[4*d].texCoords = sf::Vector2f(112.f,0);
					_vertices[4*d +1].texCoords = sf::Vector2f(128.f,0);
					_vertices[4*d +2].texCoords = sf::Vector2f(128.f,16.f);
					_vertices[4*d +3].texCoords = sf::Vector2f(112.f,16.f);
					
					break;
				}
			}
		}
	}
	std::cout << "check" << std::endl;
	for(int d = _nbCells ; d < _nbCells+nbJoints ; ++d){
		// joints
		i = (d - _nbCells) % (_width-1);
		j = (d - _nbCells) / (_width-1);
		
		x = i*16.f +8;
		y = j*16.f +8;
		
		int c00 = _map_cond[i][j];
		int c10 = _map_cond[i+1][j];
		int c01 = _map_cond[i][j+1];
		int c11 = _map_cond[i+1][j+1];
		
		bool has_detail = false;
		
		if( _map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
			(c01 == 1 or c01 == 3 or c01 == 7) and
			(c10 == 4 or c10 == 12 or c10 == 13) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(0.f,32.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(16.f,32.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(0.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 1 or c01 == 3 or c01 == 7) and
					(c10 == 5) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(16.f,32.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(32.f,32.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(32.f,48.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(16.f,48.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 1 or c11 == 3 or c11 == 11) and
					(c00 == 8 or c00 == 12 or c00 == 13) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(0.f,48.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(0.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 1 or c11 == 3 or c11 == 11) and
					(c00 == 9) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(32.f,48.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(32.f,64.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(16.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 2 or c00 == 3 or c00 == 7) and
					(c11 == 4 or c11 == 12 or c11 == 14) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(0.f,64.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(0.f,80.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 2 or c00 == 3 or c00 == 7) and
					(c11 == 6) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(32.f,64.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(32.f,80.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(16.f,80.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 2 or c10 == 3 or c10 == 11) and
					(c01 == 8 or c01 == 12 or c01 == 14) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(0.f,80.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(16.f,96.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(0.f,96.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 2 or c10 == 3 or c10 == 11) and
					(c01 == 10) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(32.f,80.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(32.f,96.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(16.f,96.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 8 or c00 == 12 or c00 == 13) and
					(c11 == 9) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(64.f,32.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(80.f,32.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(80.f,48.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(64.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 8 or c01 == 12 or c01 == 14) and
					(c10 == 10) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(96.f,48.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(112.f,48.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(112.f,64.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(96.f,64.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 4 or c10 == 12 or c10 == 13) and
					(c01 == 5) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(64.f,64.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(80.f,64.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(80.f,80.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(64.f,80.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 4 or c11 == 12 or c11 == 14) and
					(c00 == 6) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(64.f,80.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(80.f,80.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(80.f,96.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(64.f,96.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 9) and
					(c00 == 9) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(112.f,48.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(128.f,48.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(128.f,64.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(112.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 6) and
					(c00 == 6) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(112.f,80.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(128.f,80.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(128.f,96.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(112.f,96.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 5) and
					(c10 == 5) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(112.f,32.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(128.f,32.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(128.f,48.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(112.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 10) and
					(c10 == 10) ){
			has_detail = true;
			
			_vertices[4*d].texCoords = sf::Vector2f(112.f,64.f);
			_vertices[4*d +1].texCoords = sf::Vector2f(128.f,64.f);
			_vertices[4*d +2].texCoords = sf::Vector2f(128.f,80.f);
			_vertices[4*d +3].texCoords = sf::Vector2f(112.f,80.f);
		}
		
		//
		if(has_detail){
			_vertices[4*d].position = sf::Vector2f(x,y);
			_vertices[4*d +1].position = sf::Vector2f(x+16.f,y);
			_vertices[4*d +2].position = sf::Vector2f(x+16.f,y+16.f);
			_vertices[4*d +3].position = sf::Vector2f(x,y+16.f);
		}
	}
	std::cout << "recheck" << std::endl;
}
*/

void Details::SetVertices()
{
	int i, j; //coordinates of cells
	float x, y; //coordinates of quads
	float x_t, y_t; //coordinates in the texture
	int nb_x, nb_y; //number of pannels
	int nbj_x, nbj_y; // number of joints' pannels
	
	//double sprite, one centered on cells, one centered on cells' joints
	int nbJoints = (_width -1)*(_heigh -1);
	
	int CellSize = 16;
	
	nb_x = (_width / _tilesPerPannel.x) +1;
	nb_y = (_heigh / _tilesPerPannel.y) +1;
	
	nbj_x = ((_width -1) / _tilesPerPannel.x) +1;
	nbj_y = ((_heigh -1) / _tilesPerPannel.y) +1;
	
	_vertices = std::vector<std::vector<sf::VertexArray>>(nb_x + nbj_x);
	for(i = 0; i < nb_x; ++i){
		_vertices[i] = std::vector<sf::VertexArray>(nb_y);
		for(j = 0; j < nb_y; ++j){
			int pan_x, pan_y; //pannel width and heigh
			
			//pan_x = (i < nb_x-1)? _tilesPerPannel.x : _width % _tilesPerPannel.x;
			//pan_y = (j < nb_y-1)? _tilesPerPannel.y : _heigh % _tilesPerPannel.y;
			
			if(i < nb_x){
				pan_x = _tilesPerPannel.x;
				pan_y = _tilesPerPannel.y;
			}else{
				pan_x = _width % _tilesPerPannel.x;
				pan_y = _heigh % _tilesPerPannel.y;
			}
			
			_vertices[i][j] = sf::VertexArray(sf::Quads, 4*pan_x*pan_y);
		}
	}
	for(i = nb_x; i < nb_x + nbj_x; ++i){
		_vertices[i] = std::vector<sf::VertexArray>(nbj_y);
		for(j = 0; j < nbj_y; ++j){
			int pan_x, pan_y; //pannel width and heigh
			
			//pan_x = (i < nb_x + nbj_x -1)? _tilesPerPannel.x : (_width-1) % _tilesPerPannel.x;
			//pan_y = (j < nb_y + nbj_y -1)? _tilesPerPannel.y : (_heigh-1) % _tilesPerPannel.y;
			
			if(i < nbj_x){
				pan_x = _tilesPerPannel.x;
				pan_y = _tilesPerPannel.y;
			}else{
				pan_x = (_width-1) % _tilesPerPannel.x;
				pan_y = (_heigh-1) % _tilesPerPannel.y;
			}
			
			_vertices[i][j] = sf::VertexArray(sf::Quads, 4*pan_x*pan_y);
		}
	}
	
	//_vertices = sf::VertexArray(sf::Quads, 4*(_nbCells+nbJoints));
	for(int d = 0 ; d < _nbCells ; ++d){
		// borders
		i = d % _width;
		j = d / _width;
		
		// x, y : vertices' coordinates
		// x_t, y_t : problem of textures boundaries ; gotta deal with it
		x = i*CellSize;
		y = j*CellSize;
		
		if(_map_id[i][j] != 255){
		
			int condition = _map_cond[i][j];
			
			// pannel of the tile
			int pan_i, pan_j, pan_c; //coordinates
			int in_pan_i, in_pan_j; //in-pannel coordinates
			
			pan_i = i/_tilesPerPannel.x;
			pan_j = j/_tilesPerPannel.y;
			
			in_pan_i = i % _tilesPerPannel.x;
			in_pan_j = j % _tilesPerPannel.y;
			
			pan_c = ( in_pan_j * _tilesPerPannel.x ) + in_pan_i;
			
			
			if(condition != 0){
				_vertices[pan_i][pan_j][4*pan_c ].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c  +1].position = sf::Vector2f(x+16,y);
				_vertices[pan_i][pan_j][4*pan_c  +2].position = sf::Vector2f(x+16,y+16);
				_vertices[pan_i][pan_j][4*pan_c  +3].position = sf::Vector2f(x,y+16);
			}
			
			switch(condition)
			{
				case 1 : { // only above
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,0.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,0.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,16.f);
					
					break;
				}
				case 2 : { // only bellow
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,32.f);
					
					break;
				}
				case 3 : { // above & bellow
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(64.f,0.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(80.f,0.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(80.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(64.f,16.f);
					
					break;
				}
				case 4 : { // only left
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,32.f);
					
					break;
				}
				case 5 : { // above & left
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(32.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(48.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(32.f,16.f);
					
					break;
				}
				case 6 : { // bellow & left
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(64.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(48.f,32.f);
					
					break;
				}
				case 7 : { // above, bellow & left
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(112.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(112.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(96.f,32.f);
					
					break;
				}
				case 8 : { // only right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,16.f);
					
					break;
				}
				case 9 : { // above & right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(32.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(48.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(48.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(32.f,32.f);
					
					break;
				}
				case 10 : { // bellow & right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(48.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(64.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(48.f,16.f);
					
					break;
				}
				case 11 : { // above, bellow & right 
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(96.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(112.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(112.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(96.f,16.f);
					
					break;
				}
				case 12 : { // left & right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(64.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(80.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(80.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(64.f,16.f);
					
					break;
				}
				case 13 : { // above, left & right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(80.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(96.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(80.f,16.f);
					
					break;
				}
				case 14 : { // bellow, left & right
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(80.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(96.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(96.f,32.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(80.f,32.f);
					
					break;
				}
				case 16 : { // all directions
					_vertices[pan_i][pan_j][4*pan_c ].texCoords = sf::Vector2f(112.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(128.f,0);
					_vertices[pan_i][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(128.f,16.f);
					_vertices[pan_i][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(112.f,16.f);
					
					break;
				}
			}
		}
	}
	std::cout << "check" << std::endl;
	for(int d = _nbCells ; d < _nbCells+nbJoints ; ++d){
		// joints
		i = (d - _nbCells) % (_width-1);
		j = (d - _nbCells) / (_width-1);
		
		x = i*16.f +8;
		y = j*16.f +8;
		
		int c00 = _map_cond[i][j];
		int c10 = _map_cond[i+1][j];
		int c01 = _map_cond[i][j+1];
		int c11 = _map_cond[i+1][j+1];
		
		// pannel of the tile
		int pan_i, pan_j, pan_c; //coordinates
		int in_pan_i, in_pan_j; //in-pannel coordinates
		
		pan_i = i/_tilesPerPannel.x;
		pan_j = j/_tilesPerPannel.y;
		
		in_pan_i = i % _tilesPerPannel.x;
		in_pan_j = j % _tilesPerPannel.y;
		
		pan_c = ( in_pan_j * _tilesPerPannel.x ) + in_pan_i; // gérer bordure x
		
		bool has_detail = false;
		
		if( _map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
			(c01 == 1 or c01 == 3 or c01 == 7) and
			(c10 == 4 or c10 == 12 or c10 == 13) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 1 or c01 == 3 or c01 == 7) and
					(c10 == 5) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,48.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 1 or c11 == 3 or c11 == 11) and
					(c00 == 8 or c00 == 12 or c00 == 13) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 1 or c11 == 3 or c11 == 11) and
					(c00 == 9) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 2 or c00 == 3 or c00 == 7) and
					(c11 == 4 or c11 == 12 or c11 == 14) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,80.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 2 or c00 == 3 or c00 == 7) and
					(c11 == 6) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,80.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 2 or c10 == 3 or c10 == 11) and
					(c01 == 8 or c01 == 12 or c01 == 14) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(0.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(16.f,96.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(0.f,96.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 2 or c10 == 3 or c10 == 11) and
					(c01 == 10) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(16.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(32.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(32.f,96.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(16.f,96.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c00 == 8 or c00 == 12 or c00 == 13) and
					(c11 == 9) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(64.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(80.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(80.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(64.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 8 or c01 == 12 or c01 == 14) and
					(c10 == 10) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(96.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(112.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(112.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(96.f,64.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c10 == 4 or c10 == 12 or c10 == 13) and
					(c01 == 5) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(64.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(80.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(80.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(64.f,80.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 4 or c11 == 12 or c11 == 14) and
					(c00 == 6) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(64.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(80.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(80.f,96.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(64.f,96.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 9) and
					(c00 == 9) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(112.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(128.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(128.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(112.f,64.f);
		} else if( 	_map_id[i][j] != 255 and _map_id[i+1][j+1] != 255 and
					(c11 == 6) and
					(c00 == 6) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(112.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(128.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(128.f,96.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(112.f,96.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 5) and
					(c10 == 5) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(112.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(128.f,32.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(128.f,48.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(112.f,48.f);
		} else if( 	_map_id[i][j+1] != 255 and _map_id[i+1][j] != 255 and
					(c01 == 10) and
					(c10 == 10) ){
			has_detail = true;
			
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].texCoords = sf::Vector2f(112.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].texCoords = sf::Vector2f(128.f,64.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].texCoords = sf::Vector2f(128.f,80.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].texCoords = sf::Vector2f(112.f,80.f);
		}
		
		//
		if(has_detail){
			_vertices[pan_i + nb_x][pan_j][4*pan_c ].position = sf::Vector2f(x,y);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +1].position = sf::Vector2f(x+16.f,y);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +2].position = sf::Vector2f(x+16.f,y+16.f);
			_vertices[pan_i + nb_x][pan_j][4*pan_c  +3].position = sf::Vector2f(x,y+16.f);
		}
	}
	std::cout << "recheck" << std::endl;
}


void Details::draw(sf::RenderTarget & target, sf::RenderStates states) const{
	int nb_x = (_width / _tilesPerPannel.x) +1;
	
	states.texture = &_tex;
	states.transform.translate(_position);
	
	//target.draw(_vertices, states);
	
	int in_pan_x = _position.x * -1;
	in_pan_x = in_pan_x % (_tilesPerPannel.x*16); // in_pan_x : pos in pixel
	int in_pan_y = _position.y * -1;
	in_pan_y = in_pan_y % (_tilesPerPannel.y*16);
	
	//std::cout << "in_pan : : " << in_pan_x << " , " << in_pan_y << std::endl;
	int pan_x = ( (_position.x * -1) / (_tilesPerPannel.x*16)); // pan_x 
	int pan_y = ( (_position.y * -1) / (_tilesPerPannel.y*16));
	
	int nb_pan_x = 1;
	int nb_pan_y = 1;
	//std::cout << "pan : " << pan_x << " , " << pan_y << std::endl;
	if( pan_x < (_width/100)+1 and (_tilesPerPannel.x - in_pan_x) < 1280 )
		++nb_pan_x;
	if( pan_y < (_heigh/100)+1 and (_tilesPerPannel.y - in_pan_y) < 720 )
		++nb_pan_y;
	//std::cout << "nb_pan_x : " << nb_pan_x << " , " << nb_pan_y << std::endl;
	for(int i = pan_x; i < pan_x + nb_pan_x; ++i){
		for(int j = pan_y; j < pan_y + nb_pan_y; ++j){
			//std::cout << "i & j : " << i << " , " << j << std::endl;
			target.draw(_vertices[i][j], states);
			target.draw(_vertices[i + nb_x][j], states);
		}
	}
}

