#include "Entity.hpp"

Entity::Entity(){}

//##########################
// BODY MANAGMENT

void Entity::setPos(sf::Vector2f pos){_pos = pos;}
sf::Vector2f Entity::getPos(){return _pos;}

void Entity::setSize(sf::Vector2f size){_size = size;}
sf::Vector2f Entity::getSize(){return _size;}

void Entity::move(sf::Vector2f pos){_pos += pos;}

//ShapeType Entity::getShapeType(){return _type;}

//#######################
// GAME STEPS

void Entity::play(sf::Event & event, sf::Time _turnTim){}

void Entity::update(sf::Time _turnTime){}

void Entity::draw(sf::RenderWindow & window){}
