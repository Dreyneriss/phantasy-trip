#include "Dynamic_State.hpp"

WallGrab_State::WallGrab_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  _spriteCycle = std::vector<std::vector<float>>(1);
  _spriteCycle[0] = std::vector<float>{0};

  _texCoords = std::vector<std::vector<std::vector<float>>>(1);
  _texCoords[0] = std::vector<std::vector<float>>(1);
  _texCoords[0][0] = std::vector<float>{ 20, 302, 7450, 8305, -4, 16 };
  //_texCoords[0][0] = std::vector<float>{ 2542, 2864, 7450, 8305, -4, 16 };
}

//##########################
// COMMANDS

// if wallgrab toward the right
void WallGrab_State::c_left(){} // prepare to jump to the left
void WallGrab_State::c_right(){} // engage climbing
void WallGrab_State::c_up(){
  _lift = true;
} // lift the body
void WallGrab_State::c_down(){
  _grab = false;

  // if is lifted, then a simple down make him hanged and a double-tap down make him drop
} // hang or drop the body

void WallGrab_State::c_jump(){
  //direction is important :
  // jump toward the wall : climbing it quickly
  // jump opposite to the wall : walljump equivalent ?
  // jump without direction : simple jump

  if(_char != NULL){
    if(_char->canMoveUp()){
      _char->setVertSpeed(_jumpSpeed);

      _char->setState(JUMPING_STATE);
    }
  }
}

//###########################
//

void WallGrab_State::init(){
  _grab = true;
  _clock.restart();
  _time = sf::Time::Zero;

  _char->canWallGrab(_i,_j);
  sf::Vector2f pos;
  if(_char->isTexInverted())
    pos.x = _i - _char->getSize().x;// - 1;
  else
    pos.x = _i;// + 1;
  pos.y = _j + _char->getSize().y - 16;
  _char->setPos(pos);

  setSprite(0,0);
}

void WallGrab_State::update(){
  if(_grab){
    _char->setVertSpeed(0);
  }else{
    _char->setState(FALLING_STATE);
  }
  _grab = true;

  _char->move();
  _char->setHoriSpeed(0);
  _char->setVertSpeed(0);
}
