#include "Entity_Char.hpp"
#include <cmath>

// Taken from kimci86's answer in :
// https://fr.sfml-dev.org/forums/index.php?topic=22287.0
float Entity_Char::det(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return v1.x * v2.y - v1.y * v2.x;
}
float Entity_Char::dot(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return v1.x * v2.x + v1.y * v2.y;
}
float Entity_Char::squaredLength(const sf::Vector2f& v){
    return dot(v, v);
}
float Entity_Char::squaredDistance(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return squaredLength(v2 - v1);
}

bool Entity_Char::lineLine(const sf::Vector2f& A, const sf::Vector2f& B, const sf::Vector2f& C, const sf::Vector2f& D, sf::Vector2f& intersection){
    sf::Vector2f AB = B - A,
                 CD = D - C,
                 AC = C - A;

    float d = det(AB, CD);
    if(d)
    {
        intersection = A + det(AC, CD) / d * AB;
        return true;
    }
    else
        return false;
}
bool Entity_Char::segmentSegment(const sf::Vector2f& A, const sf::Vector2f& B, const sf::Vector2f& C, const sf::Vector2f& D, sf::Vector2f& intersection){
    if(lineLine(A, B, C, D, intersection))
    {
        float u = dot(intersection - A, B - A),
              v = dot(intersection - C, D - C);
        return 0.f <= u && u <= squaredDistance(A, B) &&
               0.f <= v && v <= squaredDistance(C, D);
    }
    else
        return false;
}

//

Entity_Char::Entity_Char(){
  _shape = sf::VertexArray(sf::Quads, 4);
  _shape[0].position = sf::Vector2f(0,0);
  _shape[1].position = sf::Vector2f(3,0);
  _shape[2].position = sf::Vector2f(3,-3);
  _shape[3].position = sf::Vector2f(0,-3);

  _tex.loadFromFile("./data/sprites/characters/jump/jump04.png");;
  _tex.setSmooth(true);

  _shape[3].texCoords = sf::Vector2f(0,0);
  _shape[2].texCoords = sf::Vector2f(_tex.getSize().x,0);
  _shape[1].texCoords = sf::Vector2f(_tex.getSize().x,_tex.getSize().y);
  _shape[0].texCoords = sf::Vector2f(0,_tex.getSize().y);

  _baseSpeed = 100; //pixel/sec
  _jumpSpeed = 300;
}

Entity_Char::Entity_Char(float width, float heigh, float x, float y, Instance_Units * inst){
  _inst = inst;

  _size.x = width;
  _size.y = heigh;

  //vertices and texCoords are set so that the shape's origin is to the bottom instead of the top
  // so that the origin correspond to the feet

  _shape = sf::VertexArray(sf::Quads, 4);
  _shape[0].position = sf::Vector2f(0,0);
  _shape[1].position = sf::Vector2f(_size.x,0);
  _shape[2].position = sf::Vector2f(_size.x,-_size.y);
  _shape[3].position = sf::Vector2f(0,-_size.y);

  _shape[0].position.y += 1.6;
  _shape[1].position.y += 1.6;
  _shape[0].position.x -= 1.6;
  _shape[3].position.x -= 1.6;
  _shape[1].position.x += 4.8;
  _shape[2].position.x += 4.8;


  _tex.loadFromFile("./data/sprites/characters/stand/stand1.png");
  _tex.setSmooth(true);

  _shape[3].texCoords = sf::Vector2f(0,0);
  _shape[2].texCoords = sf::Vector2f(_tex.getSize().x,0);
  _shape[1].texCoords = sf::Vector2f(_tex.getSize().x,_tex.getSize().y);
  _shape[0].texCoords = sf::Vector2f(0,_tex.getSize().y);

  setPos(sf::Vector2f(x,y));

  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  oneTapTest = false;

  _isOnFloor = false;
}

void Entity_Char::setInstance(Instance_Units* inst){
  _inst = inst;
}

//##########################
// COMMANDS

void Entity_Char::c_left(){
  _shape[0].position = sf::Vector2f(0,0);
  _shape[1].position = sf::Vector2f(_size.x,0);
  _shape[2].position = sf::Vector2f(_size.x,-_size.y);
  _shape[3].position = sf::Vector2f(0,-_size.y);

  _shape[0].position.y += 1.6;
  _shape[1].position.y += 1.6;
  _shape[0].position.x -= 1.6;
  _shape[3].position.x -= 1.6;
  _shape[1].position.x += 4.8;
  _shape[2].position.x += 4.8;

  _speed.x = -_baseSpeed;
}
void Entity_Char::c_right(){
  _shape[1].position = sf::Vector2f(0,0);
  _shape[0].position = sf::Vector2f(_size.x,0);
  _shape[3].position = sf::Vector2f(_size.x,-_size.y);
  _shape[2].position = sf::Vector2f(0,-_size.y);

  _shape[0].position.y += 1.6;
  _shape[1].position.y += 1.6;
  _shape[0].position.x += 1.6;
  _shape[3].position.x += 1.6;
  _shape[1].position.x -= 4.8;
  _shape[2].position.x -= 4.8;

  _speed.x = _baseSpeed;
}
void Entity_Char::c_up(){}
void Entity_Char::c_down(){
  if(_speed.y < -_jumpSpeed)
    _speed.y = -_jumpSpeed;
}

void Entity_Char::c_jump(){
  _speed.y = _jumpSpeed;
  _isOnFloor = false;
}

void Entity_Char::c_plus(){
  _inst->zoom(1.01);
}
void Entity_Char::c_minus(){
  _inst->zoom(0.99);
}

//##########################
// BODY MANAGMENT

void Entity_Char::setPos(sf::Vector2f pos){
  _pos0 = pos;
  _pos = pos;
}

/*
Right now, my character stay on a surface because his everfalling movement
 is converted in an horizontal movement.
I must get rid of that system because it fucks up with diagonal surfaces.
Adding flags for in-contact surfaces ?
Importing a real collision-management library ?
I don't know.
*/
// awesome tutorial :: https://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169
void Entity_Char::move(sf::Vector2f vect){
  int upt = _inst->getUnitPerTile();

  _pos0 = _pos;
  _vect1 = vect;

  sf::Vector2f shortVect = _vect1;
  sf::Vector2f normale;

  float x0, x1, y0, y1;
  int i0, i1, j0, j1;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;
  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  if(vect.x > 0){
    if(vect.y > 0){ // going down right
      if( x1 != float(int(x1)) ){ // if right not at edge of subtile (scheme 4.0?)
        float gap = float(int(x1)) - x1 +1;
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x += gap;
        }else{
          _pos.x += vect.x;
          vect.x = 0;
        }
      }
      if( y1 != float(int(y1)) ){ // if bottom not at edge of subtile (scheme 4.0?)
        float gap = float(int(y1)) - y1 +1;
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y += gap;
        }else{
          _pos.y += vect.y;
          vect.y = 0;
        }
      }

      if(!canMoveDown() or (vect.y == 0) ){
        vect.y = 0;
        move(vect);
      }else if(!canMoveRight(true) or (vect.x == 0) ){
        vect.x = 0;
        move(vect);
      }else{
        move(sf::Vector2f(vect.x,0));
        move(sf::Vector2f(0,vect.y));
      }

    }else if(vect.y < 0){ // going up right
      vect.y *= -1;
      if( x1 != float(int(x1)) ){ // if right not at edge of subtile (scheme 4.0?)
        float gap = float(int(x1)) - x1 +1;
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x += gap;
        }else{
          _pos.x += vect.x;
          vect.x = 0;
        }
      }
      if( y0 != float(int(y0)) ){ // if bottom not at edge of subtile (scheme 4.0?)
        float gap = y0 - float(int(y0));
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y -= gap;
        }else{
          _pos.y -= vect.y;
          vect.y = 0;
        }
      }

      if(!canMoveUp() or (vect.y == 0) ){
        vect.y = 0;
        move(vect);
      }else if(!canMoveRight(true) or (vect.x == 0) ){
        vect.x = 0;
        vect.y *= -1;
        move(vect);
      }else{
        move(sf::Vector2f(vect.x,0));
        move(sf::Vector2f(0,-vect.y));
      }
    }else{ // going straight right

      if( x1 != float(int(x1)) ){ // if right not at edge of subtile (scheme 4.0?)
        float gap = float(int(x1)) - x1 +1;
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x += gap;
        }else{
          _pos.x += vect.x;
          vect.x = 0;
        }
      }

      //now, either _pos.y is round or vect.y is null
      bool isFree = true;
      bool stepUp = false;
      bool stepDown = false;
      float step;
      while(vect.x > 0){
        if(vect.x<1){
          step = vect.x;
          vect.x = 0;
        }else{
          step = 1;
          vect.x -= 1;
        }
        for(int j = j0 ; j <= j1-1 ; ++j){
          if(!_inst->isSubTileFree(i1+1,j)){
            isFree = false;
            vect.x = 0;
            break;
          }
        }
        if(!_inst->isSubTileFree(i1+1,j1)){
          stepUp = true;
          for(int i = i0+1 ; i <= i1+1 ; ++i){
            if(!_inst->isSubTileFree(i,j0-1)){
              stepUp = false;
            }
          }
        }
        if(isFree){
          if(stepUp){
            _pos.y -= 1;
            j0 -= 1;
            j1 -= 1;
          }
          _pos.x += step;
          i1 += 1;
        }
      }
    }
  }else if(vect.x < 0){
    if(vect.y > 0){ // going down left
      vect.x *= -1;
      if( x0 != float(int(x0)) ){ // if left not at edge of subtile (scheme 4.0?)
        float gap = x0 - float(int(x0));
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x -= gap;
        }else{
          _pos.x -= vect.x;
          vect.x = 0;
        }
      }
      if( y1 != float(int(y1)) ){ // if bottom not at edge of subtile (scheme 4.0?)
        float gap = float(int(y1)) - y1 +1;
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y += gap;
        }else{
          _pos.y += vect.y;
          vect.y = 0;
        }
      }

      if(!canMoveDown() or (vect.y == 0) ){
        vect.x *= -1;
        vect.y = 0;
        move(vect);
      }else if(!canMoveLeft(true) or (vect.x == 0) ){
        vect.x = 0;
        move(vect);
      }else{
        move(sf::Vector2f(-vect.x,0));
        move(sf::Vector2f(0,vect.y));
      }
    }else if(vect.y < 0){ //going up left
      vect.x *= -1;
      vect.y *= -1;
      if( x0 != float(int(x0)) ){ // if left not at edge of subtile (scheme 4.0?)
        float gap = x0 - float(int(x0));
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x -= gap;
        }else{
          _pos.x -= vect.x;
          vect.x = 0;
        }
      }
      if( y1 != float(int(y1)) ){ // if bottom not at edge of subtile (scheme 4.0?)
        float gap = y1 - float(int(y1));
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y -= gap;
        }else{
          _pos.y -= vect.y;
          vect.y = 0;
        }
      }

      if(!canMoveUp() or (vect.y == 0) ){
        vect.x *= -1;
        vect.y = 0;
        move(vect);
      }else if(!canMoveLeft(true) or (vect.x == 0) ){
        vect.y *= -1;
        vect.x = 0;
        move(vect);
      }else{
        move(sf::Vector2f(-vect.x,0));
        move(sf::Vector2f(0,-vect.y));
      }
    }else{ //going straight left
      vect.x *= -1;

      if( x0 != float(int(x0)) ){ // if left not at edge of subtile (scheme 4.0?)
        float gap = x0 - float(int(x0));
        if(vect.x >= gap){
          vect.x -= gap;
          _pos.x -= gap;
        }else{
          _pos.y -= vect.y;
          vect.y = 0;
        }
      }

      //now, either _pos.y is round or vect.y is null
      bool isFree = true;
      bool stepUp = false;
      float step;
      while(vect.x > 0){
        if(vect.x<1){
          step = vect.x;
          vect.x = 0;
        }else{
          step = 1;
          vect.x -= 1;
        }
        for(int j = j0 ; j <= j1-1 ; ++j){
          if(!_inst->isSubTileFree(i0-1,j)){
            isFree = false;
            vect.x = 0;
            break;
          }
        }
        if(!_inst->isSubTileFree(i0-1,j1)){
          stepUp = true;
          for(int i = i0-1 ; i <= i1-1 ; ++i){
            if(!_inst->isSubTileFree(i,j0-1)){
              stepUp = false;
            }
          }
        }
        if(isFree){
          if(stepUp){
            _pos.y -= 1;
            j0 -= 1;
            j1 -= 1;
          }
          _pos.x -= step;
          i0 -= 1;
        }
      }
    }
  }else{ // if vect.x == 0
    if(vect.y > 0) // falling straight (Scheme 1.)
    {
      if( _pos.y != float(int(_pos.y)) ){ // if bottom not at edge of subtile (scheme 4.0?)
        float gap = float(int(_pos.y)) - _pos.y +1;
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y = float(int(_pos.y)) + 1;
        }else{
          _pos.y += vect.y;
          vect.y = 0;
        }
      }

      //now, either _pos.y is round or vect.y is null
      bool isFree = true;
      float step;
      while(vect.y > 0){
        if(vect.y<1){
          step = vect.y;
          vect.y = 0;
        }else{
          step = 1;
          vect.y -= 1;
        }
        for(int i = i0 ; i <= i1 ; ++i){
          if(!_inst->isSubTileFree(i,j1+1)){
            isFree = false;
            vect.y = 0;
            break;
          }
        }
        if(isFree){
          _pos.y += step;
          j1 += 1;
        }
      }
    }else if(vect.y < 0){ // jumping straight

      vect.y *= -1;

      if( y0 != float(int(y0)) ){ // if top not at edge of subtile (scheme 4.0?)
        float gap = float(int(y0)) - y0;
        if(vect.y >= gap){
          vect.y -= gap;
          _pos.y -= gap;
        }else{
          _pos.y -= vect.y;
          vect.y = 0;
        }
      }

      //now, either _pos.y is round or vect.y is null
      bool isFree = true;
      float step;
      while(vect.y > 0){
        if(vect.y < 1){
          step = vect.y;
          vect.y = 0;
        }else{
          step = 1;
          vect.y -= 1;
        }
        for(int i = i0 ; i <= i1 ; ++i){
          if(!_inst->isSubTileFree(i,j0-1)){
            isFree = false;
            vect.y = 0;
            _speed.y = 0;
            break;
          }
        }
        if(isFree){
          _pos.y -= step;
          j0 -= 1;
        }
      }
    }
  }
}

sf::Vector2f Entity_Char::getPos(){
  return _pos;
}

bool Entity_Char::isOnFloor(){
  return _isOnFloor;
}
void Entity_Char::setOnFloor(){}
bool Entity_Char::checkIfOnFloor(){
  return false;
}

bool Entity_Char::canMoveLeft(bool stepUp=false){ //taking in account stepping up ?
  float x0, y0, y1;
  int i0, j0, j1;

  x0 = _pos.x;
  i0 = x0;

  y0 = _pos.y - _size.y;
  j0 = y0;
  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  if(stepUp){
    j1 -= 1;
  }
  for(int j = j0 ; j <= j1 ; ++j){
    if(!_inst->isSubTileFree(i0-1,j)){
      return false;
    }
  }
  return true;
}
bool Entity_Char::canMoveRight(bool stepUp=false){
  float x1, y0, y1;
  int i1, j0, j1;

  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;
  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  if(stepUp){
    j1 -= 1;
  }
  for(int j = j0 ; j <= j1 ; ++j){
    if(!_inst->isSubTileFree(i1+1,j)){
      return false;
    }
  }
  return true;
}
bool Entity_Char::canMoveUp(){
  float x0, x1, y0;
  int i0, i1, j0;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y0 = _pos.y - _size.y;
  j0 = y0;

  for(int i = i0 ; i <= i1 ; ++i){
    if(!_inst->isSubTileFree(i,j0-1)){
      return false;
    }
  }
  return true;
}
bool Entity_Char::canMoveDown(){
  float x0, x1, y1;
  int i0, i1, j1;

  x0 = _pos.x;
  i0 = x0;
  x1 = _pos.x + _size.x;
  i1 = x1;
  if( x1 == float(int(x1)) ) //ex: if x1 == 15.0 (scheme 2.1)
    i1 -= 1;

  y1 = _pos.y;
  j1 = y1;
  if( y1 == float(int(y1)) ){
    j1 -= 1;
  }

  for(int i = i0 ; i <= i1 ; ++i){
    if(!_inst->isSubTileFree(i,j1+1)){
      return false;
    }
  }
  return true;
}


void Entity_Char::setJumpSpeed(float speed){
  _jumpSpeed = speed;
}
void Entity_Char::setBaseSpeed(float speed){
  _baseSpeed = speed;
}

sf::Vector2f Entity_Char::getSpeed(){
  return _speed;
}

void Entity_Char::setHoriSpeed(float speed){
  _speed.x = speed;
}
void Entity_Char::setVertSpeed(float speed){
  _speed.y = speed;
}

void Entity_Char::moveHoriSpeed(float speed){
  _speed.x += speed;
}
void Entity_Char::moveVertSpeed(float speed){
  _speed.y += speed;
}


/*
How to make a good collision detection (my guesses ):
The way I make it currently is collision on surfaces and not on volumes,
 which is very efficient.
The next step is to decompose the move. First it sees the closest collision detected,
 then it watches the possible move remaining and redo a collision research.
*/

// https://en.sfml-dev.org/forums/index.php?topic=1393.0
/*
usually you shouldn't compare to a specific value (ie: if(velocity==0.f)),
 instead you should use some epsilon value depending on the context,
 because it's not fixed-point, maybe if you're handling houndreds,
 epsilon could be 0.1f, but if you're handling microns, it should be 0.0000001.

You should know that floats have 6 digit significant value.

1000000.f + 1.f = 1000000.f
*/

bool Entity_Char::AABBCollision(sf::Vector2f bPos, sf::Vector2f bSize, sf::Vector2f & shortVect, sf::Vector2f & normale)
{
  //what helped me : http://zaunfish.blogspot.com/2018/03/c-and-sfml-tile-based-collision.html
  //another interesting discussion : https://en.sfml-dev.org/forums/index.php?topic=18289.0

  // body parts shortcuts

  float left0 = _pos0.x;
  float right0 = _pos0.x + _size.x;
  float above0 = _pos0.y - _size.y;
  float below0 = _pos0.y;

  float left = _vect1.x + _pos0.x;
  float right = _vect1.x + _pos0.x + _size.x;
  float above = _vect1.y + _pos0.y - _size.y;
  float below = _vect1.y + _pos0.y;

  float bLeft = bPos.x;
  float bRight = bPos.x + bSize.x;
  float bAbove = bPos.y;
  float bBelow = bPos.y + bSize.y;

  bool hitAbove = false;
  bool hitBelow = false;
  bool hitRight = false;
  bool hitLeft = false;
  float ratio;

  if(_vect1.x == 0){ //vertical movement (ex: jumping or falling)

    if( (right0 > bLeft) and (left0 < bRight) ){
      if(_vect1.y > 0){ // falling

        if( (below0 <= bAbove) and (below > bAbove)){
          hitBelow = true;
        }
      }else{ // jumping

        if( (above0 >= bBelow) and (above < bBelow) ){
          hitAbove = true;
        }
      }
    }
    else return false;
  }
  else if(_vect1.y == 0){ //horizontal movement (walking, running, planning...)

    if( (below > bAbove) and (above < bBelow) ){
      if(_vect1.x < 0){ // going left

        if( (left0 >= bRight) and (left < bRight)){
          hitLeft = true;
        }
      }else{ // going right

        if( (right0 <= bLeft) and (right > bLeft) ){
          hitRight = true;
        }
      }
    }
    else return false;
  }
  else{ //wonderful world of diagonal movements
    bool isFarAbove = (below0 < bAbove) and (below < bAbove);
    bool isFarBelow = (above0 > bBelow) and (above > bBelow);
    bool isFarLeft  = (right0 < bLeft) and (right < bLeft);
    bool isFarRight = (left0 > bRight) and (left > bRight);

    if(isFarRight or isFarLeft or isFarAbove or isFarBelow)
      return false;

    sf::Vector2f lowerLeft0 = _pos0;
    sf::Vector2f lowerRight0 = _pos0 + sf::Vector2f(_size.x,0);
    sf::Vector2f upperLeft0 = _pos0 + sf::Vector2f(0,-_size.y);
    sf::Vector2f upperRight0 = _pos0 + sf::Vector2f(_size.x,-_size.y);

    sf::Vector2f bLowerLeft = bPos + sf::Vector2f(0,bSize.y);
    sf::Vector2f bLowerRight = bPos + bSize;
    sf::Vector2f bUpperLeft = bPos;
    sf::Vector2f bUpperRight = bPos + sf::Vector2f(bSize.x,0);

    if( (_vect1.x > 0) and (_vect1.y > 0) ){ //going down right

      sf::Vector2f dist = bUpperLeft - lowerRight0; // distance between the two interesting corners of the objets

      if(dist.x < 0){ //might only hit above/below
        sf::Vector2f dist2 = bUpperRight - lowerLeft0;

        if( (_vect1.x / _vect1.y) < (dist2.x / dist2.y) ){ //else, it misses the box
          if(_vect1.y > dist2.y){ //then it hits
            hitBelow = true;
          }
        }
      }else if(dist.y < 0){ //might only hit right/left
        sf::Vector2f dist2 = bLowerLeft - upperRight0;

        if( (_vect1.y / _vect1.x) < (dist2.y / dist2.x) ){ //else, it misses the box
          if(_vect1.x > dist2.x){ //then it hits
            hitRight = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y > dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it land above the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // then the player would like to LAND rather than to FALL
            hitBelow = true;
          }else{
            hitRight = true;
          }
        }
      }

    }else if( (_vect1.x > 0) and (_vect1.y < 0) ){
      //going up right

      sf::Vector2f dist = bLowerLeft - upperRight0; // distance between the two interesting corners of the objets

      if(dist.x < 0){ //might only hit above/below
        sf::Vector2f dist2 = bLowerRight - upperLeft0;

        if( (dist2.y == 0) or ((_vect1.x / _vect1.y) > (dist2.x / dist2.y)) ){ //else, it misses the box (y values are inverted)
          if(_vect1.y < dist2.y){ //then it hits (y values inverted)
            hitAbove = true;
          }
        }
      }else if(dist.y > 0){ //might only hit right/left (y values inverted)
        sf::Vector2f dist2 = bUpperLeft - lowerRight0;

        if( (_vect1.y / _vect1.x) > (dist2.y / dist2.x) ){ //else, it misses the box (y values inverted)
          if(_vect1.x > dist2.x){ //then it hits
            hitRight = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y < dist.y) ){ //it hits (y values inverted)
          if( (_vect1.x / _vect1.y) <= (dist.x / dist.y) ){ //case where it stays below the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // would the player prefer to pass up or right ? I'd say right
            hitAbove = true;
          }else{
            hitRight = true;
          }
        }
      }
    }else if( (_vect1.x < 0) and (_vect1.y > 0) ){ //going down left

      sf::Vector2f dist = bUpperRight - lowerLeft0; // distance between the two interesting corners of the objets

      if(dist.x > 0){ //might only hit above/below (x values are inverted)
        sf::Vector2f dist2 = bUpperLeft - lowerRight0;

        if( (_vect1.x / _vect1.y) > (dist2.x / dist2.y) ){ //else, it misses the box (x values are inverted)
          if(_vect1.y > dist2.y){ //then it hits
            hitBelow = true;
          }
        }
      }else if(dist.y < 0){ //might only hit left/right
        sf::Vector2f dist2 = bLowerRight - upperLeft0;

        if( (_vect1.x / _vect1.y) < (dist2.x / dist2.y) ){ //else, it misses the box (x values are inverted)
          if(_vect1.x < dist2.x){ //then it hits
            hitLeft = true;
          }
        }
      }else{
        if( (_vect1.x < dist.x) and (_vect1.y > dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it land above the box //THAT PART BUGS ME
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // then the player would like to LAND rather than to FALL
            hitBelow = true;
          }else{
            hitLeft = true;
          }
        }
      }
    }else if( (_vect1.x < 0) and (_vect1.y < 0) ){
      //going up left

      sf::Vector2f dist = bLowerRight - upperLeft0; // distance between the two interesting corners of the objets

      if(dist.x > 0){ //might only hit above/below (x values inverted)
        sf::Vector2f dist2 = bLowerLeft - upperRight0;

        if( (dist2.y == 0) or ((_vect1.x / _vect1.y) < (dist2.x / dist2.y)) ){ //else, it misses the box
          if(_vect1.y < dist2.y){ //then it hits (y values inverted)
            hitAbove = true;
          }
        }
      }else if(dist.y > 0){ //might only hit right/left (y values inverted)
        sf::Vector2f dist2 = bUpperRight - lowerLeft0;

        if( (_vect1.x / _vect1.y) > (dist2.x / dist2.y) ){ //else, it misses the box
          if(_vect1.x < dist2.x){ //then it hits (x values inverted)
            hitLeft = true;
          }
        }
      }else{
        if( (_vect1.x > dist.x) and (_vect1.y < dist.y) ){ //it hits
          if( (_vect1.x / _vect1.y) >= (dist.x / dist.y) ){ //case where it stays below the box
            //note: if (vect.x / vect.y) == (dist2.x / dist2.y),
            // would the player prefer to pass up or left ? I'd say left
            hitAbove = true;
          }else{
            hitLeft = true;
          }
        }
      }
    }
  }

  if(hitAbove){
    shortVect.y = bBelow - above0;
    ratio = shortVect.y / _vect1.y;
    shortVect.x = _vect1.x * ratio;

    normale.x = 0;
    normale.y = 1;

    return true;
  }else if(hitBelow){
    shortVect.y = bAbove - below0;
    ratio = shortVect.y / _vect1.y;
    shortVect.x = _vect1.x * ratio;

    normale.x = 0;
    normale.y = -1;

    return true;
  }else if(hitRight){
    shortVect.x = bLeft - right0;
    ratio = shortVect.x / _vect1.x;
    shortVect.y = _vect1.y * ratio;

    normale.x = -1;
    normale.y = 0;

    return true;
  }else if(hitLeft){
    shortVect.x = bRight - left0;
    ratio = shortVect.x / _vect1.x;
    shortVect.y = _vect1.y * ratio;

    normale.x = 1;
    normale.y = 0;

    return true;
  }

  return false;
}

void Entity_Char::play(sf::Event & event, sf::Time _turnTime){

  //#####################
  // Initial Situation :
  // - If keys have been pressed, Command methods have already been used
  // - There must be a flag system,
  // so that going NEast isn't just a sum of going North and going East
  // - There might be a hierarchy in commands.
  // Direction commands are grouped, but what about jumping, sprinting and hitting ?
  // - There must be a state system.
  // Jumping while in ground or while in air is QUITE different.


  //loadListOfCommands(event);
  // flags needed :
  // - what key is already holding
  // - what action is in course
  // - state (falling)
  // types of commands :
  // - Cooldown vs Nonstop
  // - Press-Release vs Spammable

  //prise en compte des actions types : course, marche, saut, attaques...

  //gestion des déplacements et collisions avec les autres entités
  //envoie des effets aux entités concernés, pour la partie update
  //si immobile, écartement des entités empilés (magnétisme)

  /*float delta = 100 * _turnTime.asSeconds() * -1;
    _shape.move(0,delta);
  float delta = 100 * _turnTime.asSeconds();
    _shape.move(0,delta);
  float delta = 100 * _turnTime.asSeconds() * -1;

    _shape.move(delta,0);
  float delta = 100 * _turnTime.asSeconds();
    _shape.move(delta,0);*/
}

void Entity_Char::update(sf::Time _turnTime){
  sf::Vector2f vect;
  vect.x += _speed.x*_turnTime.asSeconds();
  vect.y += _speed.y*_turnTime.asSeconds();

  if((vect.x != 0) or (vect.y != 0))
    move(vect);
  _speed.x = 0;
  if(!canMoveDown())
    _speed.y = 0;
}

void Entity_Char::draw(sf::RenderTarget & target, sf::RenderStates states) const{
  //states.transform.translate(_pos*_inst->getZoom());
  states.transform.translate(_inst->getScreenCenter());
  states.transform.scale(_inst->getZoom(),_inst->getZoom());
  states.texture = &_tex;
  //std::cout << _pos.x << " , " << _pos.y << std::endl;
  //target.draw(_shape, std::size_t firstVertex, std::size_t vertexCount, states);
  target.draw(_shape, states);
}
