#include "Terrain.hpp"

Terrain::Terrain(World * world): _world(world){
	_tilesPerPannel = sf::Vector2i(100,100);
}

void Terrain::SetTexture(std::string adress)
{
	_tex.loadFromFile(adress);
	_tex.setRepeated(true);
}
void Terrain::SetName(std::string name)
{
	_name = name;
}
void Terrain::SetId(int id)
{
	_id = id;
}

void Terrain::SetVertices(int width, int heigh)
{
	_width = width;
	_heigh = heigh;
	_nbCells = width*heigh;
	int i, j; //coordinates of cells
	float x, y; //coordinates of quads
	float x_t, y_t; //coordinates in the texture

	int CellSize = _world->getUnitPerTile();

	_pannelWidth = (_width / _tilesPerPannel.x) +1; // nb of pannels
	_pannelHeigh = (_heigh / _tilesPerPannel.y) +1;

	_vertices = std::vector<std::vector<sf::VertexArray>>(_pannelWidth);
	for(i = 0; i < _pannelWidth; ++i){
		_vertices[i] = std::vector<sf::VertexArray>(_pannelHeigh);
		for(j = 0; j < _pannelHeigh; ++j){
			int pan_x, pan_y; //pannel width and heigh

			//pan_x = (i < _pannelWidth-1)? _tilesPerPannel.x : _width % _tilesPerPannel.x;
			//pan_y = (j < _pannelHeigh-1)? _tilesPerPannel.y : _heigh % _tilesPerPannel.y;

			if(i < _pannelWidth){
				pan_x = _tilesPerPannel.x;
				pan_y = _tilesPerPannel.y;
			}else{
				pan_x = _width % _tilesPerPannel.x;
				pan_y = _heigh % _tilesPerPannel.y;
			}

			_vertices[i][j] = sf::VertexArray(sf::Quads, 4*pan_x*pan_y);
		}
	}

	for(int c = 0; c < _nbCells; ++c){
		// cells coordinates
		i = c % _width;
		j = c / _width;

		// x, y : vertices' coordinates
		x = i*CellSize;
		x_t = x;

		y = j*CellSize;
		y_t = y;

		if(_world->getIdAt(i,j) == _id){

			int condition = _world->getTypeAt(i,j);

			// pannel of the tile
			int pan_i, pan_j, pan_c; //coordinates
			int in_pan_i, in_pan_j; //in-pannel coordinates
			int pan_x, pan_y; //in_nb of tiles ?

			pan_i = i/_tilesPerPannel.x;
			pan_j = j/_tilesPerPannel.y;

			in_pan_i = i % _tilesPerPannel.x;
			in_pan_j = j % _tilesPerPannel.y;

			pan_c = ( in_pan_j * _tilesPerPannel.x ) + in_pan_i;

			switch(condition)
			{
				case 5 : { // above & left : NWest triangle
					_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

					_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

					break;
				}
				case 6 : { // bellow & left : SWest triangle
					_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
					_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y);

					_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
					_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t);

					break;
				}
				case 9 : { // above & right : NEast triangle
					_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
					_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x,y);
					_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

					_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
					_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t,y_t);
					_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

					break;
				}
				case 10 : { // bellow & right : SEast triangle
					_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
					_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

					_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
					_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

					break;
				}
				default : { // normal case
					_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
					_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
					_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

					_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
					_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
					_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
					_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

					break;
				}
			}
		}
	}
}

void Terrain::updateVerticesAt(int i, int j)
{
	float x, y; //coordinates of quads
	float x_t, y_t; //coordinates in the texture

	int CellSize = _world->getUnitPerTile();

	if(_world->getIdAt(i,j) == _id){ // there is one Terrain per texture
		int condition = _world->getTypeAt(i,j);

		// x, y : vertices' coordinates
		x = i*CellSize;
		x_t = x;

		y = j*CellSize;
		y_t = y;

		// pannel of the tile
		int pan_i, pan_j, pan_c; //coordinates
		int in_pan_i, in_pan_j; //in-pannel coordinates
		int pan_x, pan_y; //in_nb of tiles ?

		pan_i = i/_tilesPerPannel.x;
		pan_j = j/_tilesPerPannel.y;

		in_pan_i = i % _tilesPerPannel.x;
		in_pan_j = j % _tilesPerPannel.y;

		pan_c = ( in_pan_j * _tilesPerPannel.x ) + in_pan_i;

		switch(condition)
		{
			case 5 : { // above & left : NWest triangle
				_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

				_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

				break;
			}
			case 6 : { // bellow & left : SWest triangle
				_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y);

				_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
				_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t);

				break;
			}
			case 9 : { // above & right : NEast triangle
				_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

				_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
				_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t,y_t);
				_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

				break;
			}
			case 10 : { // bellow & right : SEast triangle
				_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

				_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
				_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

				break;
			}
			default : { // normal case
				_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(x,y);
				_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(x+CellSize,y);
				_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(x+CellSize,y+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(x,y+CellSize);

				_vertices[pan_i][pan_j][4*pan_c].texCoords = sf::Vector2f(x_t,y_t);
				_vertices[pan_i][pan_j][4*pan_c +1].texCoords = sf::Vector2f(x_t+CellSize,y_t);
				_vertices[pan_i][pan_j][4*pan_c +2].texCoords = sf::Vector2f(x_t+CellSize,y_t+CellSize);
				_vertices[pan_i][pan_j][4*pan_c +3].texCoords = sf::Vector2f(x_t,y_t+CellSize);

				break;
			}
		}
	}else{
		x = i*CellSize;
		x_t = x;

		y = j*CellSize;
		y_t = y;

		// pannel of the tile
		int pan_i, pan_j, pan_c; //coordinates
		int in_pan_i, in_pan_j; //in-pannel coordinates
		int pan_x, pan_y; //in_nb of tiles ?

		pan_i = i/_tilesPerPannel.x;
		pan_j = j/_tilesPerPannel.y;

		in_pan_i = i % _tilesPerPannel.x;
		in_pan_j = j % _tilesPerPannel.y;

		pan_c = ( in_pan_j * _tilesPerPannel.x ) + in_pan_i;

		_vertices[pan_i][pan_j][4*pan_c].position = sf::Vector2f(0,0);
		_vertices[pan_i][pan_j][4*pan_c +1].position = sf::Vector2f(0,0);
		_vertices[pan_i][pan_j][4*pan_c +2].position = sf::Vector2f(0,0);
		_vertices[pan_i][pan_j][4*pan_c +3].position = sf::Vector2f(0,0);
	}
}


void Terrain::draw(sf::RenderTarget & target, sf::RenderStates states) const{
	sf::Vector2f focus = _world->getFocusedPos();
	sf::Vector2f recenter = _world->getScreenCenter() - (focus * _world->getZoom());

	states.texture = &_tex;
	states.transform.translate(recenter);
  states.transform.scale(_world->getZoom(),_world->getZoom());

	int pan_x = focus.x / (_tilesPerPannel.x*_world->getUnitPerTile());
	int pan_y = focus.y / (_tilesPerPannel.y*_world->getUnitPerTile());

	if(pan_x == 0)
		++pan_x;
	else if(pan_x == (_pannelWidth -1))
		--pan_x;

	if(pan_y == 0)
		++pan_y;
	else if(pan_y == (_pannelHeigh -1))
		--pan_y;

	for(int i = pan_x-1; i <= pan_x + 1; ++i){
		for(int j = pan_y-1; j <= pan_y + 1; ++j){
			target.draw(_vertices[i][j], states);
		}
	}
}
