#include "BackGround.hpp"

BackGround::BackGround(std::vector<std::vector<int>> & map_id, std::vector<std::vector<int>> & map_cond, int & width, int & heigh, int & nbcell, sf::Vector2f & pos) : _map_id(map_id), _map_cond(map_cond), _width(width), _heigh(heigh), _nbCells(nbcell), _position(pos)
{}

//~Terrain(); //TODO

void BackGround::move(int x, int y)
{
  //_position.x += x;
  //_position.y += y;
}


void BackGround::SetTexture(std::string adress)
{
	_tex.loadFromFile(adress);
}
void BackGround::SetName(std::string name)
{
	_name = name;
}
void BackGround::SetId(int id)
{
	_id = id;
}

void BackGround::SetVertices()
{
	_vertices = sf::VertexArray(sf::Quads, 4);
	
	_vertices[0].position = sf::Vector2f(0,0);
	_vertices[1].position = sf::Vector2f(_width,0);
	_vertices[2].position = sf::Vector2f(_width,_tex.getSize().y);
	_vertices[3].position = sf::Vector2f(0,_tex.getSize().y);
	
	_vertices[0].texCoords = sf::Vector2f(0,0);
	_vertices[1].texCoords = sf::Vector2f(_tex.getSize().x,0);
	_vertices[2].texCoords = sf::Vector2f(_tex.getSize().x,_tex.getSize().y);
	_vertices[3].texCoords = sf::Vector2f(0,_tex.getSize().y);
}


void BackGround::draw(sf::RenderTarget & target, sf::RenderStates states) const{
	states.texture = &_tex;
	
	float pos_max;
	pos_max = _heigh*16;
	float ratio = _position.y / pos_max;
	float pos = _tex.getSize().y * ratio;
	
	states.transform.translate(0,pos);
	
	target.draw(_vertices, states);
}
