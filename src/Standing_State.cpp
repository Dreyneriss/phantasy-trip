#include "Dynamic_State.hpp"

Standing_State::Standing_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  _spriteCycle = std::vector<std::vector<float>>(2);
  _spriteCycle[0] = std::vector<float>{0.f};
  //_spriteCycle[1] = std::vector<float>{0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f};
  _spriteCycle[1] = std::vector<float>{0.15f, 0.3f, 0.45f, 0.60f, 0.75f, 0.80f, 0.95f, 1.10f};
  //_spriteCycle[1] = std::vector<float>{0.2f, 0.4f, 0.6f, 0.8f, 1.0f, 1.2f, 1.4f, 1.6f};
  //_spriteCycle[1] = std::vector<float>{0.4f, 0.8f, 1.2f, 1.6f, 2.0f, 2.4f, 2.8f, 3.2f};

  _texCoords = std::vector<std::vector<std::vector<float>>>(2);
  _texCoords[0] = std::vector<std::vector<float>>(1);
  _texCoords[0][0] = std::vector<float>{ 53, 368, 0, 814, -5.5, 1 };
  _texCoords[1] = std::vector<std::vector<float>>(8);
  _texCoords[1][0] = std::vector<float>{ 0, 401, 957, 1774, -12, 1 };
  _texCoords[1][1] = std::vector<float>{ 435, 747, 962, 1739, -5, 2.5 };
  _texCoords[1][2] = std::vector<float>{ 754, 987, 960, 1717, -1, 1 };
  _texCoords[1][3] = std::vector<float>{ 1075, 1347, 960, 1713, -4, 0.5 };
  _texCoords[1][4] = std::vector<float>{ 1396, 1833, 959, 1773, -11, 3 };
  _texCoords[1][5] = std::vector<float>{ 1877, 2195, 959, 1768, -5, 3 };
  _texCoords[1][6] = std::vector<float>{ 2197, 2389, 960, 1773, 0, 3 };
  _texCoords[1][7] = std::vector<float>{ 2515, 2817, 960, 1761, -5.5, 3 };
}

//##########################
// COMMANDS

void Standing_State::c_left(){
  _walking = true;

  if(_char != NULL){
    _char->setHoriSpeed(-_baseSpeed);
    _char->setTexInvertion(false);
  }
}
void Standing_State::c_right(){
  _walking = true;

  if(_char != NULL){
    _char->setHoriSpeed(_baseSpeed);
    _char->setTexInvertion(true);
  }
}
void Standing_State::c_up(){}
void Standing_State::c_down(){
  if(_char != NULL){
    if(_char->getSpeed().y < -_jumpSpeed)
      _char->setVertSpeed(-_jumpSpeed);
  }
}

void Standing_State::c_jump(){
  if(_char != NULL){
    if(_char->canMoveUp()){
      _char->setVertSpeed(_jumpSpeed);

      _char->setState(JUMPING_STATE);
    }
  }
}

//###########################
//

void Standing_State::init(){
  _clock.restart();
  _time = sf::Time::Zero;

  setSprite(0,0);

  _walking = false;
}

void Standing_State::update(){
  if(_char->canMoveDown()){
    if(_char->getSpeed().y < 0){
      _char->setState(JUMPING_STATE);
    }else{
      _char->setState(FALLING_STATE);
    }
  }
  if(_walking){
    float t_x0,t_x1,t_y0,t_y1;
    float width, heigh, offset_x, offset_y;
    _walking = false;
    _time += _clock.restart();
    int sprite = _spriteCycle[1].size();
    for(int i=0; i<_spriteCycle[1].size(); ++i){
      if(_time.asSeconds() < _spriteCycle[1][i]){
        sprite = i;
        break;
      }
    }
    if(sprite == _spriteCycle[1].size()){
      sprite = 0;
      _time = sf::Time::Zero;
    }
    setSprite(1,sprite);
  }else{
    _time = sf::Time::Zero;
    setSprite(0,0);
  }

  _char->moveOnFloor();

  _char->setHoriSpeed(0);
  _char->setVertSpeed(0);
}
