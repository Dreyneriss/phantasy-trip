#include "Dynamic_State.hpp"

Falling_State::Falling_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  _spriteCycle = std::vector<std::vector<float>>(1);
  _spriteCycle[0] = std::vector<float>{0.15f, 0.3f, 0.45f};
  //_spriteCycle[1] = std::vector<float>{0.2f, 0.4f, 0.6f};
  //_spriteCycle[1] = std::vector<float>{0.4f, 0.8f, 1.2f};

  _texCoords = std::vector<std::vector<std::vector<float>>>(1);
  _texCoords[0] = std::vector<std::vector<float>>(3);
  _texCoords[0][0] = std::vector<float>{ 393, 731, 2877, 3683, -4.5, 0 };
  _texCoords[0][1] = std::vector<float>{ 766, 1371, 2877, 3583, -13.5, 2.5 };
  _texCoords[0][2] = std::vector<float>{ 1439, 1978, 2878, 3524, -10.5, 1 };
}

//##########################
// COMMANDS

void Falling_State::c_left(){
  if(_char != NULL){
    _char->setHoriSpeed(-_baseSpeed);
    _char->setTexInvertion(false);
  }
}
void Falling_State::c_right(){
  if(_char != NULL){
    _char->setHoriSpeed(_baseSpeed);
    _char->setTexInvertion(true);
  }
}
void Falling_State::c_up(){
  if(_char != NULL){
    if( (_char->isTexInverted() and !_char->canMoveRight()) or (!_char->isTexInverted() and !_char->canMoveLeft()) ){
      if(_char->canWallGrab()){
        _char->setState(WALLGRAB_STATE);
        _char->getState(AIRJUMPING_STATE)->setAvailable(true);
        _char->getState(WALLJUMPING_STATE)->setAvailable(true);
      }
    }
  }
}
void Falling_State::c_down(){
  if(_char != NULL){
    if(_char->getSpeed().y < -_jumpSpeed)
      _char->setVertSpeed(-_jumpSpeed);
  }
}

void Falling_State::c_jump(){
  if(_char != NULL){
    if( (_char->isTexInverted() and !_char->canMoveRight()) or (!_char->isTexInverted() and !_char->canMoveLeft()) ){
      if(_char->getState(WALLJUMPING_STATE)->isAvailable()){ //canWallRunLow/High
        _char->setState(WALLJUMPING_STATE);
      }
    }else{
      sf::Time jumpTime = _char->getState(JUMPING_STATE)->getTime();
      bool avail = _char->getState(AIRJUMPING_STATE)->isAvailable();
      bool ready = _char->getState(JUMPING_STATE)->getTime() <= sf::Time::Zero;
      if(avail and ready){
        _char->setState(AIRJUMPING_STATE);
        _char->getState(AIRJUMPING_STATE)->setAvailable(false);
      }
    }
  }
}

//###########################
//

void Falling_State::init(){
  _clock.restart();
  _time = sf::Time::Zero;

  setSprite(0,0);
}

void Falling_State::update(){
  if(!_char->canMoveDown()){
    _char->setState(STANDING_STATE);
    _char->getState(AIRJUMPING_STATE)->setAvailable(true);
    _char->getState(WALLJUMPING_STATE)->setAvailable(true);
  }
  _time += _clock.restart();
  int sprite = _spriteCycle[0].size()-1;
  for(int i=0; i<_spriteCycle[0].size(); ++i){
    if(_time.asSeconds() < _spriteCycle[0][i]){
      sprite = i;
      break;
    }
  }
  setSprite(0,sprite);

  _char->move();
  _char->setHoriSpeed(0);
}
