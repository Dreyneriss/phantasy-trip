#include "Dynamic_State.hpp"

WallJumping_State::WallJumping_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -25*16;

  _duration = sf::seconds(0.25f);
  _cooldown = sf::seconds(0.15f);

  _spriteCycle = std::vector<std::vector<float>>(1);
  _spriteCycle[0] = std::vector<float>{0.f};

  _texCoords = std::vector<std::vector<std::vector<float>>>(1);
  _texCoords[0] = std::vector<std::vector<float>>(1);
  _texCoords[0][0] = std::vector<float>{ 2109, 2460, 2877, 3771, -8, 8 };
}

//##########################
// COMMANDS

void WallJumping_State::c_left(){
  if(_char != NULL){
    //_char->setHoriSpeed(-_baseSpeed);
    _char->moveHoriSpeed(-_baseSpeed/20);
    //_char->setTexInvertion(false);
  }
}
void WallJumping_State::c_right(){
  if(_char != NULL){
    //_char->setHoriSpeed(_baseSpeed);
    _char->moveHoriSpeed(_baseSpeed/20);
    //_char->setTexInvertion(true);
  }
}
void WallJumping_State::c_up(){}
void WallJumping_State::c_down(){
  if(_char != NULL){
    if(_char->getSpeed().y < -_jumpSpeed){
      _char->setVertSpeed(-_jumpSpeed);
      _char->setState(FALLING_STATE);
    }
  }
}

void WallJumping_State::c_jump(){}

//###########################
//

void WallJumping_State::setAvailable(bool available){
  _available = available;
  _first = available;
}
bool WallJumping_State::isAvailable(){
  _time -= _clock.restart();
  return _available and (_time <= sf::Time::Zero);
}

void WallJumping_State::init(){
  _char->setVertSpeed(_jumpSpeed);
  float horiSpeed = _jumpSpeed/3;
  if(!_char->isTexInverted())
    horiSpeed *= -1;
  _char->setHoriSpeed(horiSpeed);

  _clock.restart();
  _time = sf::Time::Zero;

  if(_first){
    setSprite(0,0);
  }else{
    setSprite(0,0);
  }
}

void WallJumping_State::update(){
  _time += _clock.restart();
  if( (_time <= _duration) and _char->canMoveUp() and _char->canMoveDown() ){

    if(_first){
      setSprite(0,0);
    }else{
      setSprite(0,0);
    }

  }else if(_char->canMoveDown()){
    if(_first){
      _first = false;
    }else{
      _available = false;
    }
    _clock.restart();
    _time = _cooldown;

    _char->setState(FALLING_STATE);
  }else{
    setAvailable(true);
    _char->setState(STANDING_STATE);
  }

  _char->move();
  if(!_char->canMoveUp() and _char->getSpeed().y<0)
    _char->setVertSpeed(0);
}
