#include "Dynamic_State.hpp"

Jumping_State::Jumping_State(){
  _baseSpeed = 8*16; //unit/sec
  _jumpSpeed = -15*16;

  _limit = sf::seconds(0.5f);
  _cooldown = sf::seconds(0.3f);

  _spriteCycle = std::vector<std::vector<float>>(1);
  _spriteCycle[0] = std::vector<float>{0.f};

  _texCoords = std::vector<std::vector<std::vector<float>>>(1);
  _texCoords[0] = std::vector<std::vector<float>>(1);
  _texCoords[0][0] = std::vector<float>{ 85, 332, 2875, 3786, -3.5, 8 };
}

//##########################
// COMMANDS

void Jumping_State::c_left(){
  if(_char != NULL){
    _char->setHoriSpeed(-_baseSpeed);
    _char->setTexInvertion(false);
  }
}
void Jumping_State::c_right(){
  if(_char != NULL){
    _char->setHoriSpeed(_baseSpeed);
    _char->setTexInvertion(true);
  }
}
void Jumping_State::c_up(){}
void Jumping_State::c_down(){
  if(_char != NULL){
    if(_char->getSpeed().y < -_jumpSpeed)
      _char->setVertSpeed(-_jumpSpeed);
  }
}

void Jumping_State::c_jump(){
  _jumping = true;
}

//###########################
//

sf::Time Jumping_State::getTime(){
  _time -= _clock.restart();
  return _time;
}

void Jumping_State::init(){
  _clock.restart();
  _time = sf::Time::Zero;
  _jumping = true;

  setSprite(0,0);
}

void Jumping_State::update(){
  if(_jumping){
    _time += _clock.restart();
    if(_time <= _limit){
      setSprite(0,0);
      if(_char != NULL){
        _char->setVertSpeed(_jumpSpeed);
      }
    }else{
      _time = _cooldown;
      _clock.restart();
      _char->setState(FALLING_STATE);
    }
  }else{
    _time = _cooldown;
    _clock.restart();
    _char->setState(FALLING_STATE);
  }
  _jumping = false;

  _char->move();
  _char->setHoriSpeed(0);
  if(!_char->canMoveUp() and _char->getSpeed().y<0)
    _char->setVertSpeed(0);
}
