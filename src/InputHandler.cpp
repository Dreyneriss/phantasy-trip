#include "InputHandler.hpp"

InputHandler::InputHandler()
{
  _cJump = new JumpCommand();
  _cUp = new UpCommand();
  _cDown = new DownCommand();
  _cRight = new RightCommand();
  _cLeft = new LeftCommand();
  _cPlus = new PlusCommand();
  _cMinus = new MinusCommand();
  _cRClic = new RightClicCommand();
  _cLClic = new LeftClicCommand();
  _cMClic = new MiddleClicCommand();
  _cT1Clic = new Thumb1ClicCommand();
  _cT2Clic = new Thumb2ClicCommand();
}

InputHandler::~InputHandler()
{
  delete(_cJump);
  delete(_cUp);
  delete(_cDown);
  delete(_cRight);
  delete(_cLeft);
  delete(_cPlus);
  delete(_cMinus);
  delete(_cRClic);
  delete(_cLClic);
  delete(_cMClic);
  delete(_cT1Clic);
  delete(_cT2Clic);

  _cJump = NULL;
  _cUp = NULL;
  _cDown = NULL;
  _cRight = NULL;
  _cLeft = NULL;
  _cPlus = NULL;
  _cMinus = NULL;
  _cRClic = NULL;
  _cLClic = NULL;
  _cMClic = NULL;
  _cT1Clic = NULL;
  _cT2Clic = NULL;

  _keyZ = NULL;
  _keyQ = NULL;
  _keyS = NULL;
  _keyD = NULL;
  _keyUp = NULL;
  _keyDown = NULL;
  _keyRight = NULL;
  _keyLeft = NULL;
  _keySpace = NULL;
  _keyShift = NULL;
  _keyCtrl = NULL;
  _keyEntr = NULL;
  _keyPlus = NULL;
  _keyMinus = NULL;
  _mouseRight = NULL;
  _mouseLeft = NULL;
  _mouseMiddle = NULL;
  _mouseThumb1 = NULL;
  _mouseThumb2 = NULL;
}

void InputHandler::handleInput(Playable* actor)
{
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    _keyZ->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    _keyS->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    _keyQ->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    _keyD->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    _keyUp->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    _keyDown->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    _keyRight->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    _keyLeft->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    _keySpace->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add))
    _keyPlus->execute(actor);

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
    _keyMinus->execute(actor);

  //clics de la souris
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		_mouseLeft->execute(actor);

	if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		_mouseRight->execute(actor);

	if (sf::Mouse::isButtonPressed(sf::Mouse::Middle))
		_mouseMiddle->execute(actor);

	if (sf::Mouse::isButtonPressed(sf::Mouse::XButton1))
		_mouseThumb1->execute(actor);

	if (sf::Mouse::isButtonPressed(sf::Mouse::XButton2))
		_mouseThumb2->execute(actor);
}

void InputHandler::initDefault(){
  _keyZ = _cUp;
  _keyUp = _cUp;
  _keyS = _cDown;
  _keyDown = _cDown;
  _keyD = _cRight;
  _keyRight = _cRight;
  _keyQ = _cLeft;
  _keyLeft = _cLeft;
  _keyPlus = _cPlus;
  _keyMinus = _cMinus;
  _mouseRight = _cRClic;
  _mouseLeft = _cLClic;
  _mouseMiddle = _cMClic;
  _mouseThumb1 = _cT1Clic;
  _mouseThumb2 = _cT2Clic;

  _keySpace = _cJump;

}
