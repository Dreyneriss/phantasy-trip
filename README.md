# Phantasy Trip

Now I have the begining of a physic.

Upcomming tasks :

- Going on with Dynamic_States, make it smoother and cleaner, imagining states, transitions and animations
  - Adding sprints, and maybe airsprint ?
- Playing with Perlin noise and other procedural map generation
- Parkour physics
  - feet size ? Nope.
  - running on walls : 2 times, with hands them with feets, or with feets then with feets
  - grabing borders
- Backgrounds, lights
- More elaborate tiles : have an array per special tile in order to check instantly a subTile accessibility
- Level creation
  - With the mouse,
  - ability to change id and type,
  - ability to save in a file
- additional character
  - possibility to switch contrôle,
  - ability to contrôle from distance,
  - basic AI that try to follow you or can follow basic orders
  - basic social actions, automatically done when close enouth
- Qt interface
  - mini-map
  - choice between tiles
  - other characters, social notifications, effects ?

I feel quite numb right now. Setting simple priorities and simple objectives seems like a necessity.

- Studying Perlin noise and other procedural map generation
  - make it work even on Python.
- Work on Parkour physics
  - enhance the wallgrab
  - make him run
- Work on AI
  - basic AI that try to follow you or can follow basic orders
  - basic social actions, automatically done when close enouth
- Make Qt work, even on another project if it's more simple to start with


For the collision managment, I used (or yet not) these websites :
- https://www.reddit.com/r/gamemaker/comments/4nqqzs/any_idea_about_the_magic_behind_terrarias_great/
- http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
- "2d collision separating axis theorem"
- https://www.youtube.com/watch?v=1r1rElIiWqw : help a lot, just for the general idea !
  - also : "I've found that in the absence of slopes (where the only geometry the player interacts with are square walls) this technique adds a kind of "mantling" mechanic, where if you jump for a platform, but hit its vertical edge a few pixels below the corner, the player is boosted up to the top surface of said platform. It feels as though it smoothens out fast platforming gameplay. Thanks for the tutorial Shaun! "
- https://gamedev.stackexchange.com/questions/141218/how-exactly-do-slopes-and-edges-work-in-this-scenario
- http://danjb.com/game_dev/tilebased_platformer_slopes
- http://danjb.com/game_dev/tilebased_platformer_slopes_2
- https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-7-slopes-groundwork--cms-28472 : also helped a lot for the idea
- https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-8-slopes--cms-28704
- also interesting to keep in view : https://news.ycombinator.com/item?id=6365706 ; https://bp.io/post/1501
